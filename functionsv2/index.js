'use strict';
const functions = require('@google-cloud/functions-framework');

// const firebasefunctions = require('firebase-functions');

// const escapeHtml = require('escape-html');

const pgmTransactionId = '1JgmzjVO6oSTbgK3V_p9zJXokA9rxNsG0XEFvfRb-WI4';

// [START functions_firebase_reactive]
const Firestore = require('@google-cloud/firestore');
const moment = require('moment-timezone');

const firestore = new Firestore({
  projectId: process.env.GOOGLE_CLOUD_PROJECT,
});

// setup for OauthCallback
const DB_TOKEN_PATH = '/api_tokens';
// OAuth token cached locally.
let oauthTokens = null;

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
// admin.initializeApp();
admin.initializeApp({
  serviceAccountId: 'firebase-adminsdk-wun9f@babelasia-37615.iam.gserviceaccount.com',
  apiKey: "AIzaSyBoucAnhK3TCPMaJ5QrNnLlw7-bCjsjyoo",
  authDomain: "babelasia-37615.firebaseapp.com",
  databaseURL: "https://babelasia-37615.firebaseio.com",
  projectId: "babelasia-37615",
  storageBucket: "babelasia-37615.appspot.com",
  messagingSenderId: "171899057367",
  appId: "1:171899057367:web:853f0f7baadebbda24ec92",
  measurementId: "G-6GSZPHL0NH"
});
// To set the timestampsInSnapshots, to avoid the error/warning message from firestore (timestamp format changes);
const settings = {/* your settings... */ timestampsInSnapshots: true};
admin.firestore().settings(settings);

// write to spreadsheets
// const {OAuth2Client} = require('google-auth-library');
// const {google} = require('googleapis');
// const sheets = google.sheets('v4');

// const key = require('./config/key.json');

// setup for authGoogleAPI
// const FUNCTIONS_REDIRECT = `https://us-central1-babelasia-37615.cloudfunctions.net/oauthcallback`;
// const CONFIG_CLIENT_ID = functions.config().googleapi.client_id;
// const CONFIG_CLIENT_SECRET = functions.config().googleapi.client_secret;
// const functionsOauthClient = new OAuth2Client(CONFIG_CLIENT_ID, CONFIG_CLIENT_SECRET,
//     FUNCTIONS_REDIRECT);


functions.http('helloHttpV2', (req, res) => {
 res.send(`Hello ${req.query.name || req.body.name || 'World'}!`);
});

functions.http('helloGET', (req, res) => {
    res.send({success:true, message: 'helloGET'});
});


// // checks if oauthTokens have been loaded into memory, and if not, retrieves them
// async function getAuthorizedClient() {
//     if (oauthTokens) {
//       return functionsOauthClient;
//     }
//     const snapshot = await admin.database().ref(DB_TOKEN_PATH).once('value');
//     oauthTokens = snapshot.val();
//     functionsOauthClient.setCredentials(oauthTokens);
//     return functionsOauthClient;
// }

// function updateGoogleSheet(requestWithoutAuth) {
//     return new Promise((resolve, reject) => {
//       return getAuthorizedClient().then((client) => {
//         const request = requestWithoutAuth;
//         request.auth = client;
//         // console.log('client request: ', client);
//         return sheets.spreadsheets.values.batchUpdate(request, (err, response) => {
//           // console.log('sheetRequest: ', request);
//           if (err) {
//             console.log(`The update API returned an error: ${err}`);
//             return reject(err);
//           }
//           else{
//             // console.log('Update', response.data);
//             return resolve(response.data);
//           }
//         });
//       }).catch(error=>{
//         console.log('updateGoogleSheeterror: ', error);
//         return reject(error);
//       })
//     });
// }

/**
 * Background Function triggered by a change to a Firestore document.
 *
 * @param {!Object} event The Cloud Functions event.
 * @param {!Object} context Cloud Functions event metadata.
 */
 exports.helloFirestore = (event, context) => {
    const triggerResource = context.resource;
  
    console.log(`Function triggered by event on: ${triggerResource}`);
    console.log(`Event type: ${context.eventType}`);
  
    if (event.oldValue && Object.keys(event.oldValue).length) {
      console.log('\nOld value:');
      console.log(JSON.stringify(event.oldValue, null, 2));
    }
  
    if (event.value && Object.keys(event.value).length) {
      console.log('\nNew value:');
      console.log(JSON.stringify(event.value, null, 2));
    }
  };

  const timestamp = admin.firestore.FieldValue.serverTimestamp();

function getTheDate(theDate){
    if (theDate === null){return}
    // for timestamp firebase
    if (typeof(theDate)==='object'){return theDate.toDate()}
    // for string date format
    else if (typeof(theDate)==='string'){return new Date(theDate)}
}

function getTheDateFormat(theDate, format = null){
  if (theDate === null){return ''}
  // for timestamp firebase
  if (typeof(theDate)==='object'){
    return !format? moment(theDate.toDate()).tz('Asia/Kuala_Lumpur').format('YYYY-MM-DD h:mm a'):moment(theDate.toDate()).tz('Asia/Kuala_Lumpur').format(format) 
  }
  // for string date format
  else if (typeof(theDate)==='string'){
    return !format? moment(new Date(theDate)).tz('Asia/Kuala_Lumpur').format('YYYY-MM-DD h:mm a'):moment(new Date(theDate)).tz('Asia/Kuala_Lumpur').format(format)
  }
}

  function getMembershipStart (userData){
    if (userData){
        return (userData.autoMembershipStarts? userData.autoMembershipStarts:userData.membershipStarts?userData.membershipStarts:null);
    }
    else {return null}
  }
  
  function getMembershipEnd (userData){
    if (userData){
        return (userData.autoMembershipEnds? userData.autoMembershipEnds:userData.membershipEnds?userData.membershipEnds:null);
    }
    else {return null}
  }

  function getLastName (userData){
    var userLastName = 'not found';
    const lastName = userData && userData.lastName;
    // console.log(`lastNamelength: ${lastName.length}`);
    // const nameArraySpace1 = userData.name && userData.name.split(' ');
    // console.log('nameArraySpace: ', nameArraySpace1);
  
    if (userData){
      if (lastName && lastName.length>1){
        userLastName = lastName;
      }
      else{
          const name = userData && userData.name && userData.name.trim();
  
          if (name && name.length>2 && name.toLowerCase().includes('binti ')){
              const nameArray = name.toLowerCase().split('binti');
              // console.log('nameArray: ', nameArray);
              userLastName = nameArray[1]
          }
          else if (name && name.length>2 && name.toLowerCase().includes('bin ')){
            const nameArray = name.toLowerCase().split('bin');
            userLastName = nameArray[1];
          }
          else if (name && name.length>2 && name.toLowerCase().includes('bt ')){
            const nameArray = name.toLowerCase().split('bt');
            userLastName = nameArray[1];
          }
          else if (name && name.length>2 && name.toLowerCase().includes('b ')){
            const nameArray = name.toLowerCase().split('b ');
            userLastName = nameArray[1];
          }
          else if (name && name.length>2 && name.toLowerCase().includes('b. ')){
            const nameArray = name.toLowerCase().split('b. ');
            userLastName = nameArray[1];
          }
          // contains space, count how many space
          else{
              const nameArraySpace = name && name.split(' ');
              // console.log('nameArraySpace: ', nameArraySpace);
              if (nameArraySpace && nameArraySpace.length===2){
                  userLastName = nameArraySpace[1];
              }
              else if (nameArraySpace && nameArraySpace.length>2){
                  // userLastName = nameArraySpace[2];
                  userLastName = (nameArraySpace.slice(2)).join(' ');
              }
              else{
                  userLastName = name; // testing only
              }
          }
      }
    }
    // console.log('userLastName fn: ', userLastName);
    return userLastName;
  }


//   exports.readGoogleSheet = (event, context) => {
//     const client = new google.auth.JWT(key.client_email, null, key.private_key, [
//       'https://www.googleapis.com/auth/spreadsheets.readonly'
//     ]);
  
//     const readSheet = async auth => {
//       const gsapi = google.sheets({
//         version: 'v4',
//         auth
//       });
  
//       const options = {
//         spreadsheetId: process.env.SPREADSHEETID,
//         range: 'sheet1!A:C'
//       };
//       const { data } = await gsapi.spreadsheets.values.get(options);
//       console.log(data.values);
//     };
  
//     client.authorize((err, tokens) => {
//       if (err) {
//         console.error(err);
//         return;
//       }
//       readSheet(client);
//     });
//   };
//   exports.readGoogleSheet();


functions.http('addalltransactionstopgmsheets', (req, res)=>{

    const itemData = req.body;
    const userEmail = itemData && itemData.email;
    const userId = itemData && itemData.userId;
    const userQuery = userEmail? admin.firestore().collection('users').where('email', '==', userEmail).get():admin.firestore().collection('users').get();
    // const packageQuery = admin.firestore().collection('packages').get();
    const paymentQuery = userId? admin.firestore().collection('payments').where('type', '==', 'membership').
        where('userId', '==', userId).get():
        admin.firestore().collection('payments').where('type', '==', 'membership')
        // .limit(100)
        // .where('status', '==', 'CLOSED')
        .get();
    const monthlyPackageQuery = admin.firestore().collection('pgmPackages').get();
    const pgmUserQuery = admin.firestore().collection('pgmUsers').where('userType', '==', 'Member').get();
    const pgmContractQuery = admin.firestore().collection('pgmContracts').get();

    // return res.status(200).send({success:true});

    const used1 = process.memoryUsage().heapUsed / 1024 / 1024;  
    console.log(`at this point (start), The script uses approximately ${Math.round(used1 * 100) / 100} MB`);

    let name = 'test';

  switch (req.get('content-type')) {
    // '{"name":"John"}'
    case 'application/json':
      ({name} = req.body);
      break;

    // 'John', stored in a Buffer
    case 'application/octet-stream':
      name = req.body.toString(); // Convert buffer to a string
      break;

    // 'John'
    case 'text/plain':
      name = req.body;
      break;

    // 'name=John' in the body of a POST request (not the URL)
    case 'application/x-www-form-urlencoded':
      ({name} = req.body);
      break;
  }

  return Promise.all([userQuery, 
    paymentQuery, monthlyPackageQuery, 
    pgmUserQuery, pgmContractQuery
  ]).then(results=>{
    const used2 = process.memoryUsage().heapUsed / 1024 / 1024;  
    console.log(`at this point (2), The script uses approximately ${Math.round(used2 * 100) / 100} MB`);

    const userRes = results[0];
    // const pkgRes = results[1];
    const paymentRes = results[1];
    const monthlyPkgRes = results[2];
    const pgmUserRes = results[3];
    const pgmContractRes = results[4];

    var staffMap = {};
    var userSheets = [];
    var transSheet = [];
    var pkgMap = {};
    var paymentMap = {};
    var paymentArray = [];

    var pgmUserMapByFBUserId = {};
    var pgmUserMapByPGMUserId = {};
    pgmUserRes.forEach(doc=>{
      const data = doc.data();
      const fbUserId = data.fbUserId;
      const userId = data.userId;
      if (fbUserId){
        pgmUserMapByFBUserId[fbUserId] = data;
        pgmUserMapByPGMUserId[userId] = data;
      }
    });

    // pgmContractMap by pgm userId
    var pgmContractMapByUserId = {};
    var pgmContractArray = [];
    var pgmContractMap = {};
    pgmContractRes.forEach(doc=>{
      const data = doc.data();
      pgmContractMap[doc.id]=data;
      const pgmUserId = data.userId;
      const pgmUserData = pgmUserId && pgmUserMapByPGMUserId[pgmUserId];
      const pgmFBUserId = pgmUserData && pgmUserData.fbUserId;
      const userHomeClubId = data.userHomeClubId;
      const contractId = data.contractId;

      if (pgmUserId && userHomeClubId && (userHomeClubId === 1||userHomeClubId === 2)){
        pgmContractArray = pgmContractMapByUserId[pgmUserId]||[];
        pgmContractArray.push(data);
        pgmContractMapByUserId[pgmUserId]=pgmContractArray;
      }
      // pgmContractMapByUserId[doc.id]=data;
    });

    var monthlyPkgMap = {}
    var monthlypkgMapByPrice = {};
    var monthlyPkgArray = [];
    monthlyPkgRes.forEach(doc=>{
      const data = doc.data();
      const packageIds = data.packageIds;
      const price = data.price && parseFloat(data.price).toFixed(2);
      monthlypkgMapByPrice[price]=data;
      monthlypkgMapByPrice[price].id = doc.id;
      monthlyPkgMap[doc.id]=data;
      // packageIds && packageIds.forEach(packageId=>{
      //   monthlyPkgMap[]
      // });
    });

    var paymentMapByContractId = {};
    var paymentArrayByContract = [];
    // var transactionInfoArray = [];
    var transactionInfoMap = {};

    // paymentRes.forEach(doc=>{
    //     const data = doc.data();
    //     const userId = data.userId;
    //     const status = data.status;
    //     const freezeFor = data.freezeFor;
    //     const source = data.source;
    //     const type = data.type;
    //     const pgmUserData = userId && pgmUserMapByFBUserId[userId];
    //     const pgmUserId = pgmUserData && pgmUserData.userId;
    //     const pgmContractData = pgmUserId && pgmContractMapByUserId[pgmUserId];
    //     const totalPrice = data.totalPrice;
  
    //     if (((type && type==='membership') && (status && status === 'CLOSED')) 
    //         && !freezeFor
    //         // || (freezeFor) 
    //        // || (source && (source === 'join' || source === 'luckyDraw' || source === 'promo' || source === 'free' || source === 'complimentary' || source === 'jfr' || source === 'refer'))
    //        && (pgmContractData && pgmContractData.length>0)
    //        && (totalPrice && parseInt(totalPrice)>0)
    //        ){
    //     //   paymentArray = paymentMap[userId] || [];
    //     //   paymentArray.push({...data, paymentId:doc.id});
    //     //   paymentMap[userId]=paymentArray;

    //       // paymentArrayByContract = paymentMapByContractId[]
    //       pgmContractData && pgmContractData.forEach(pgmCont=>{
    //         const contractStartDate = pgmCont.contractStartDate;
    //         const contractId = pgmCont.contractId;
    //         if (contractStartDate && contractId){
    //              // todo, compare with date...
    //             transactionInfoArray.push(pgmCont, data);
    //             // 
    //         }
    //       });

    //     }
    //   });
  
    paymentRes.forEach(doc=>{
        const data = doc.data();
        const userId = data.userId;
        const status = data.status;
        const freezeFor = data.freezeFor;
        const source = data.source;
        const type = data.type;
        const pgmUserData = userId && pgmUserMapByFBUserId[userId];
        const pgmUserId = pgmUserData && pgmUserData.userId;
        const pgmContractData = pgmUserId && pgmContractMapByUserId[pgmUserId];
  
        if (((type && type==='membership') && (status && status === 'CLOSED')) 
            && pgmContractData 
            // || (freezeFor) 
            || (source && (source === 'join' || source === 'luckyDraw' || source === 'promo' || source === 'free' || source === 'complimentary' || source === 'jfr' || source === 'refer'))
          ){
          paymentArray = paymentMap[userId] || [];
          paymentArray.push({...data, paymentId:doc.id});
          paymentMap[userId]=paymentArray;
        }
      });

      var userPaymentDetails = {};
      var userContractDetails = {};
  
      const used3 = process.memoryUsage().heapUsed / 1024 / 1024;  
      console.log(`at this point (3), The script uses approximately ${Math.round(used3 * 100) / 100} MB`);

    
    //   userRes.forEach(doc=>{
    //     const data = doc.data();
    //     const pgmUserData = (doc.id && pgmUserMapByFBUserId[doc.id]);
    //     const pgmUserId = pgmUserData && pgmUserData.userId;

    //     const paymentData = paymentMap[doc.id];
      
    //     // get pgmcontract for each user
    //     const pgmUserContractDataArray = pgmUserId && pgmContractMapByUserId[pgmUserId];

    //     if (pgmUserContractDataArray && pgmUserContractDataArray.length>0 && paymentData && paymentData.length>0 && pgmUserId){
    //         paymentData.forEach(payData=>{
    //             const userId = payData.userId;
                
    //         });

    //         pgmUserContractDataArray.forEach(pgmContractData=>{
    //             transactionInfoArray.push(pgmContractData);
    //         });
    //     }
    //   });

      userRes && userRes.forEach(doc=>{
        const data = doc.data();
        const packageId = data && data.packageId;
        const email = data && data.email;
        const name = data && data.name;
        const lastName = getLastName(data);
        // const gender = data && data.gender;
        const firstJoinVisit = data && data.firstJoinVisit;
        const clubId = firstJoinVisit? (firstJoinVisit === 'TTDI')? 101 : (firstJoinVisit === 'KLCC')? 102 : 101 : 101;
        // const gantnerCardNumber = data && data.gantnerCardNumber;
        const membershipEnds = data && getMembershipEnd(data);
        const membershipStarts = data && getMembershipStart(data);
        const isActive = packageId && membershipEnds && membershipStarts && moment(getTheDate(membershipEnds)).isSameOrAfter(moment());
        const isGuest = !membershipEnds && !membershipStarts;
        const isMember = (packageId && membershipEnds && membershipStarts)? true:false;
        // const automaticRenew = isActive? 1:0;
        // const mcId = data && data.mcId;
        // const consultantData = mcId && staffMap[mcId];
        // const consultantEmail = consultantData && consultantData.email;
        const remarks = data && data.remarks;
        const isStaff = data && data.isStaff;
        const roles = data && data.roles;
        const staffRole = data && data.staffRole;
        const paymentData = paymentMap[doc.id];
        const isTestEmail = email && (email.toLowerCase().includes('faizul') || email.toLowerCase().includes('billy'));
        const pgmUserData = pgmUserMapByFBUserId[doc.id];
        const pgmUserId = pgmUserData && pgmUserData.userId;
        // const pgmUserContractsData = pgmUserData && pgmUserData.userId && pgmContractMapByUserId[pgmUserData.userId];
        const pgmUserContractsData = pgmUserId && pgmContractMapByUserId[pgmUserId];
  
        // not inclde duplicate name
        if (isMember 
          && name 
          && !isStaff
          && !roles
          && !staffRole
          && !isGuest
          && paymentData
          && !isTestEmail
          && pgmUserContractsData
          && (email && email === 'liliankohlb@yahoo.com' || email === 'j.panterbrick@gmail.com' || email === 'aishaashikin.yaacob@gmail.com')
        ){
          
           // sort the payment
           paymentData && paymentData.sort((a,b)=>{
            const createdA = a.createdAt;
            const createdB = b.createdAt;
            if(createdA < createdB){return 1}
            else if(createdB < createdA){return -1;}
            else{return 0}
          });
          paymentData && paymentData.reverse();
  
          var monthStartCount = 0;
          var monthEndCount = 1;
          var startMoment = moment(getTheDate(membershipStarts)).tz('Asia/Kuala_Lumpur').startOf('day');
          var endMoment = moment(getTheDate(membershipEnds)).tz('Asia/Kuala_Lumpur').startOf('day');
  
          var userFreezes = [];
          var userFreezeTerminated = [];
          var userFreeAccess = [];
          var membershipHistoryList = [];
          var combinedData = [];
          var combinedVendMth = [];
          var combinedTransactions = [];
          var addMonths = 0;
          var addYears = 0;
          
          var paymentCount = 0;
          paymentData && paymentData.forEach((payData, index)=>{
            const freezeFor = payData.freezeFor;
            const packageId = payData.packageId;
            const paymentCreatedDate = payData.createdAt;
            const renewalTerm = (payData && payData.renewalTerm) || 'month';
            const source = payData.source;
            const qty = payData.quantity||1;
            const cardSummary = payData.cardSummary;
            const cardExpired = payData.cardExpiryDate;
            const price = payData.totalPrice? payData.totalPrice:0;
            const status = payData.status;
            const type = payData.type;
            const paymentType = payData.paymentType;
            const userId = payData.userId;
            const paymentId = payData.paymentId;
         
            // 1. need to sort the freeze first
            if (source && freezeFor){
              const freezeType = payData.freezeType;
              for (var a = 0; a<qty; a++){
                userFreezes.push({
                  date:moment(getTheDate(freezeFor)).add(a, 'months'),
                  freezeType,
                  yearOfFreeze: moment(freezeFor).format('YYYY'),
                  cardSummary, cardExpired, paymentId,
                  ...payData,
                  isFreeze:true
                  // need to link with contract?
                });
              }
            }
            else if (source==='freezeTerminate'){
              userFreezeTerminated.push({date:moment(freezeFor), paymentId});
            }
            else if ((source==='join') || (source==='luckyDraw') || (source==='promo')|| (source==='free')
            || (source==='complimentary') || (source==='jfr') || (source==='refer') 
            || (parseInt(price)===0)
            ){
              userFreeAccess.push({
                date:moment(getTheDate(paymentCreatedDate)), type:source, paymentId, 
                ...payData,
                isFree:true
              });
            }
  
            else if (((source === 'vend') || ((source === 'adyen') && (parseInt(price)!=0)) || (source==='pbonline'))
            && (status === 'CLOSED') && (type === 'membership')){
              for (var h=0; h<qty; h++){
                combinedTransactions.push({
                  ...payData,
                  paymentId,
                  cycle:h+1
                })
                // combinedVendMth.push({
                //   date:moment(paymentCreatedDate).add(i, 'months'), 
                //   paymentDate:paymentCreatedDate,
                //   type:source,
                //   price, paymentType, cardSummary, cardExpired,
                //   packageId, paymentId, renewalTerm, 
                //   ...payData
                // });
              }
              // for the combined transactions
             
              var monthlyPkgPrice = 0;
              var monthlyPkgData;
              if (renewalTerm && (renewalTerm === 'year' || renewalTerm === 'yearly')){
                monthlyPkgPrice = price && parseFloat(price/12*qty).toFixed(2);
                monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
                const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
                const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
      
                for (var i=0; i<(qty*12); i++){
                  combinedVendMth.push({
                    date:moment(getTheDate(paymentCreatedDate)).add(i, 'months'), 
                    paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                    type:source,
                    cycle:i+1,
                    ...payData,
                    ...monthlyPkgData,
                    monthlyPkgId,
                    monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                    // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                  });
                  // if (cardSummary){
                  //   combinedVendMth[i].cardSummary = `${cardSummary} ${cardExpired}`;
                  // }
                }
              }
              else if (renewalTerm && (renewalTerm === 'month' || renewalTerm === 'monthly')){
                monthlyPkgPrice = price && parseFloat(price/qty).toFixed(2);
                monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
                const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
                const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
                for (var j=0; j<qty; j++){
                  combinedVendMth.push({
                    date:moment(getTheDate(paymentCreatedDate)).add(j, 'months'),
                    paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                    type:source,
                    cycle:j+1,
                    ...payData,
                    ...monthlyPkgData,
                    monthlyPkgId,
                    monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                    // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=220)? true:false
                    isFree: (price && parseInt(price) === 0)? true:false
                  });
                  // if (cardSummary){
                  //   combinedVendMth[j].cardSummary = `${cardSummary} ${cardExpired}`;
                  // }
                }
              }
              else if (renewalTerm && (renewalTerm === 'biyearly' || renewalTerm === 'biyear')){
                monthlyPkgPrice = price && parseFloat(price/6*qty).toFixed(2);
                monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
                const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
                const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
                for (var k=0; k<qty*6; k++){
                  combinedVendMth.push({
                    date:moment(getTheDate(paymentCreatedDate)).add(k, 'months'), 
                    paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                    cycle:k+1,
                    ...payData,
                    ...monthlyPkgData,
                    monthlyPkgId,
                    monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                    // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                  });
                  // if (cardSummary){
                  //   combinedVendMth[k].cardSummary = `${cardSummary} ${cardExpired}`;
                  // }
                }
              }
              else if (renewalTerm && renewalTerm === 'quarterly'){
                monthlyPkgPrice = price && parseFloat(price/3*qty).toFixed(2);
                monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
                const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
                const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
                for (var l=0; l<qty*3; l++){
                  combinedVendMth.push({
                    date:moment(getTheDate(paymentCreatedDate)).add(l, 'months'), 
                    paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                    cycle:l+1,
                    ...payData,
                    ...monthlyPkgData,
                    monthlyPkgId,
                    monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                    // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                  });
                  // if (cardSummary){
                  //   combinedVendMth[l].cardSummary = `${cardSummary} ${cardExpired}`;
                  // }
                }
              }
              else if (renewalTerm && renewalTerm === '4monthly'){
                monthlyPkgPrice = price && parseFloat(price/4*qty).toFixed(2);
                monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
                const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
                const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
                for (var m=0; m<qty*4; m++){
                  combinedVendMth.push({
                    date:moment(getTheDate(paymentCreatedDate)).add(m, 'months'), 
                    paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                    cycle:m+1,
                    ...payData,
                    ...monthlyPkgData,
                    monthlyPkgId,
                    monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                    // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                  });
                  // if (cardSummary){
                  //   combinedVendMth[m].cardSummary = `${cardSummary} ${cardExpired}`;
                  // }
                }
              }
              // for complimentary
              else if (renewalTerm && renewalTerm === 'never'){
  
              }
            }
  
            // resort the array dates
            userFreezes.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
            userFreezes.reverse();
            userFreezeTerminated.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
            userFreezeTerminated.reverse();
            userFreeAccess.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
            userFreeAccess.reverse();
            combinedVendMth.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
            combinedVendMth.reverse();
  
            const initialMonthsDiff = Math.max(moment(new Date()).diff(startMoment, 'months'));
            var monthsDiff = initialMonthsDiff;
            var totalArrayLength = userFreezeTerminated.length + userFreezes.length + userFreeAccess.length + combinedVendMth.length;
            if (totalArrayLength>initialMonthsDiff){
              monthsDiff = totalArrayLength-1;
            }
  
            // default, if there is no payment detected
            for (var i=0; i<=monthsDiff; i++){
              const iterationStartMoment = startMoment.clone().add(i, 'months');
              // combinedItems.push({effectiveDate:iterationStartMoment, primaryText:primaryText, secondaryText: secondaryText, action:action});
              membershipHistoryList.push({
                iterationDate:iterationStartMoment.format('YYYY-MM-DD'), 
                count:i,
                userId
                // addMonths, addYears,
                // startMoment:startMoment && startMoment.format('YYYY-MM-DD')
              });
            }
  
            membershipHistoryList && membershipHistoryList.forEach((x,indexx)=>{
              if (userFreezeTerminated && userFreezeTerminated.length>0 
                && moment(x.iterationDate).isSameOrAfter(userFreezeTerminated[userFreezeTerminated.length-1].date)
                // && moment(userFreezeTerminated[userFreezeTerminated.length-1].date).isBetween(x.date, moment(x.date).add(1,'month'))
                // && moment(x.date).isBetween(userFreezeTerminated[userFreezeTerminated.length-1].date, userFreezeTerminated[userFreezeTerminated.length-1].date.add('months', 1))
              ){
                  combinedData.push({
                    date:userFreezeTerminated[userFreezeTerminated.length-1].date,
                    type:'freezeTerminated',
                    index:indexx,
                    ...userFreezeTerminated[userFreezeTerminated.length-1]
                  });
                  userFreezeTerminated.pop();
              }
              else if (userFreezes && userFreezes.length>0
                && moment(x.iterationDate).isSameOrAfter(userFreezes[userFreezes.length-1].date.clone()) 
                && moment(x.iterationDate).isBefore(userFreezes[userFreezes.length-1].date.clone().add(1, 'months')) 
                // && moment(userFreezes[userFreezes.length-1].date).isBetween(x.date, (x.date).add('month', 1).subtract('days', 1))
                // && moment(x.date).isBetween(userFreezes[userFreezes.length-1].date, userFreezes[userFreezes.length-1].date.add('months', 1))
                ){
                combinedData.push({
                  // date:userFreezes[userFreezes.length-1].date,
                  // type:'freeze',
                  // freezeType: userFreezes[userFreezes.length-1].freezeType? userFreezes[userFreezes.length-1].freezeType:null,
                  // price:userFreezes[userFreezes.length-1].price, 
                  // freezeCountPerYear:userFreezes[userFreezes.length-1].freezeCountPerYear,
                  index:indexx,
                  // paymentId:userFreezes[userFreezes.length-1].paymentId,
                  ...userFreezes[userFreezes.length-1]
                });
                userFreezes.pop();
              }
              else if (userFreeAccess && userFreeAccess.length>0 
                // && userFreeAccess[userFreeAccess.length-1].date.isSameOrAfter(moment(x.date))
                && moment(x.iterationDate).isSameOrAfter(userFreeAccess[userFreeAccess.length-1].date)
                // && userFreeAccess[userFreeAccess.length-1].date.isBetween(x.date, moment(x.date).add(1,'month'))
                ){
                combinedData.push({
                  // date:userFreeAccess[userFreeAccess.length-1].date,
                  // type:userFreeAccess[userFreeAccess.length-1].type,
                  index:indexx,
                //  paymentId:userFreeAccess[userFreeAccess.length-1].paymentId,
                 ...userFreeAccess[userFreeAccess.length-1]
                })
                userFreeAccess.pop();   
              }
              else if (combinedVendMth && combinedVendMth.length>0){
                combinedData.push({
                  // date:combinedVendMth[combinedVendMth.length-1].date,
                  // paymentDate: combinedVendMth[combinedVendMth.length-1].paymentDate,
                  // type:combinedVendMth[combinedVendMth.length-1].type,
                  // // visitLeft:combinedVendMth[combinedVendMth.length-1].visitLeft,
                  // // visitMax: combinedVendMth[combinedVendMth.length-1].visitMax,
                  // price:combinedVendMth[combinedVendMth.length-1].price, 
                  // paymentType: combinedVendMth[combinedVendMth.length-1].paymentType,
                  // cardSummary: combinedVendMth[combinedVendMth.length-1].cardSummary,
                  // cardExpired: combinedVendMth[combinedVendMth.length-1].cardExpired,
                  // index:indexx,
                  // packageId:combinedVendMth[combinedVendMth.length-1].packageId,
                  // paymentId:combinedVendMth[combinedVendMth.length-1].paymentId,
                  // renewalTerm: combinedVendMth[combinedVendMth.length-1].renewalTerm,
                  index:indexx,
                  ...combinedVendMth[combinedVendMth.length-1]
                })
                combinedVendMth.pop();
              }
             
              else{
                // combinedData.push({
                //   ...x,
                //   test: 'outside of loop',
                //   iterationDate:x.iterationDate
                // })
              }
            });
  
            // sorting...
            combinedData.sort((a,b) => {
              const totalPriceA = a.totalPrice? parseInt(a.totalPrice):0;
              const totalPriceB = b.totalPrice? parseInt(a.totalPrice):0;
            //   if (a.date>b.date){return 1}
            //   else if (a.date<b.date){return -1}
              const dateA = a.date.format('YYYY-MM-DD');
              const dateB = b.date.format('YYYY-MM-DD');
              if (dateA>dateB){
                return 1
                // if (totalPriceA>totalPriceB){
                //   return 1;
                // }
                // else if (totalPriceA<totalPriceB){
                //   return -1;
                // }
                // else{
                //   return 1
                // }
              }
              else if (dateA<dateB){
                return -1;
                // if (totalPriceA>totalPriceB){
                //   return -1;
                // }
                // else if (totalPriceA<totalPriceB){
                //   return 1;
                // }
                // else{
                //   return -1
                // }
              }
              else if (dateA === dateB){
                if (totalPriceA>totalPriceB){
                  return 1;
                }
                else if (totalPriceA<totalPriceB){
                  return -1;
                }
                else{
                  return 0
                }
              }
              else {
                return 0
              }
  
              // if (dateA === dateB)
              // a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD')
              // const createdA = a.createdAt;
              // const createdB = b.createdAt;
              // if(createdA < createdB){return 1}
              // else if(createdB < createdA){return -1;}
              // else{return 0}
            });
            combinedTransactions.sort((a,b)=>a.createdAt - b.createdAt);
  
          });
          
          var contractLength = 0;
          var refMonthCount = 0;
          var paidMonthCount = 0;
          var paymentsByContract = [];
          var contractpackageID = null;
          var contractCounter = 0;
          var freezeCount = 0;
          var freeMonthCount = 0;
          var startDate;
          var endDate;
          var transactionsByContract=[];
          var combinedTotalPrice = 0;
  
          pgmUserContractsData.forEach(pgmContracts=>{
            const contractId=pgmContracts.contractId;
            const contractStartDate = pgmContracts.contractStartDate;
            const contractEndDate = pgmContracts.contractEndDate;
  
            if (contractId){
              // if (!userPaymentDetails[doc.id]){
              //   userPaymentDetails[doc.id]={};
              // }
              // if (!userPaymentDetails[doc.id][contractId]){
              //   userPaymentDetails[doc.id][contractId]=
              //   {
              //     ...pgmContracts,
              //   }
              // }
              if (!userContractDetails[doc.id]){
                userContractDetails[doc.id]={};
              }
              if (!userContractDetails[doc.id][contractId]){
                userContractDetails[doc.id][contractId]=
                {
                  ...pgmContracts,
                  monthlyPkgId:contractpackageID,
                  packageId:'', // packageId of the current contract
                  startDate:startDate, // start date of the current contract
                  endDate:endDate, // endDate of the current contract
                  payments:[], // payments for the contract
                  // contractLength: 0, // month count
                  freezeCount:0,
                  freeMonthCount:0,
                  paidMonthCount:0,
                  refMonthCount:0,
                  combinedTotalPrice:0,
                }
              }
              userContractDetails[doc.id][contractId]={
                ...pgmContracts,
                monthlyPkgId:contractpackageID,
                packageId:'', // packageId of the current contract
                startDate:startDate, // start date of the current contract
                endDate:endDate, // endDate of the current contract
                payments:[], // payments for the contract
                // contractLength: 0, // month count
                freezeCount:0,
                freeMonthCount:0,
                paidMonthCount:0,
                refMonthCount:0,
                combinedTotalPrice:0,
              }
            }
          });
  
          membershipHistoryList.forEach((x, indexx, array)=>{
            // // compare with contract id here...
            // Object.entries(userContractDetails).forEach(([userId1, contractValues1]) => {
            //     Object.entries(contractValues1).forEach(([contractId, contractValue1]) => {

            //     });
            // });

            if (!userPaymentDetails){
              userPaymentDetails={}
            }
            if (!userPaymentDetails[doc.id]){
              userPaymentDetails[doc.id]={
                totalContracts:0
              }; // test first
            }
            if (!userPaymentDetails[doc.id]['totalContracts']){
              userPaymentDetails[doc.id]['totalContracts'] = 0;
            }
          
            if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
              // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                packageId:'', // packageId of the current contract
                monthlyPkgId:'',
                startDate:'', // start date of the current contract
                endDate:'', // endDate of the current contract
                payments:[], // payments for the contract
                contractLength: 0, // month count
                freezeCount:0,
                freeMonthCount:0,
                paidMonthCount:0,
                refMonthCount:0,
                combinedTotalPrice:0,
                // monthlyPkgPrice:0
              }; // default
            }
            if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']){
              // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']=[]; // default
            }
  
            userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['userId'] = x.userId;
            userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['userData'] = data;
            userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['userData']['lastName'] = lastName;
  
            if (!startDate){ // default

              startDate = moment(x.iterationDate).format('YYYY-MM-DD');
            }
  
            if (combinedData && combinedData.length>0 && indexx<combinedData.length){
              if (combinedData[indexx].totalPrice && parseFloat(combinedData[indexx].totalPrice)>0 && !combinedData[indexx].isFree){
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['combinedTotalPrice'] += parseFloat(combinedData[indexx].totalPrice);
              }
  
              if (combinedData[indexx].monthlyPkgId && (!contractpackageID || (contractpackageID===combinedData[indexx].monthlyPkgId))
                && (!combinedData[indexx].isFree) && (!(combinedData[indexx].type && combinedData[indexx].type === 'freezeTerminated'))
                ){
                if (!contractpackageID){
                  contractpackageID = combinedData[indexx].monthlyPkgId;
                  startDate = startMoment.format('YYYY-MM-DD');
                }
                if ((startDate && contractLength === 0) || !startDate){ // for the next contract
                  startDate = moment(x.iterationDate).format('YYYY-MM-DD');
                }
                if (!endDate){
                  endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                  endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                
                // endDate = moment(startDate).add(contractLength+1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                // if (moment(x.iterationDate).isAfter(startDate) && ){
                //   startDate = x.date;
                // }
                contractLength++;
                paidMonthCount++;
                
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = startDate;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['packageId'] = contractpackageID;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['monthlyPkgId'] = contractpackageID;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['paidMonthCount'] = paidMonthCount;
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`][`monthlyPkgPrice`]
                paymentsByContract.push({
                  // date:array[indexx].date,
                  ...x,
                  ...combinedData[indexx],
                  payType:'paid',
                  paidMonthCount,
                  endDate
                });
                transactionsByContract.push({
                  ...combinedTransactions[indexx]
                });
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['transactions'] = transactionsByContract;
                // remove the combinedData to avoid repetition
                // combinedData.splice(indexx, 1); 
              }
              // for referral
              else if(!combinedData[indexx].monthlyPkgId && combinedData[indexx].source && combinedData[indexx].source === 'refer'){
                refMonthCount++;
                contractLength++
                paymentsByContract.push({
                  ...x,
                  ...combinedData[indexx],
                  payType:'refer',
                  endDate
                });
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['refMonthCount'] = refMonthCount;
                if (!endDate){
                  endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                  endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
              }
              // for free
              else if ((combinedData[indexx].monthlyPkgId && combinedData[indexx].totalPrice && (parseInt(combinedData[indexx].totalPrice)===0 && contractpackageID && combinedData[indexx].isFree))
                || (combinedData[indexx].totalPrice && parseInt(combinedData[indexx].totalPrice)===0 && combinedData[indexx].source && combinedData[indexx].source != 'freeze')
                || (contractpackageID && combinedData[indexx].isFree)
              ){ // for free, use the current contract packageId
                paymentsByContract.push({
                  ...x,
                  ...combinedData[indexx],
                  payType:'free',
                  startDate,
                  endDate
                });
                if (!userPaymentDetails){
                  userPaymentDetails={}
                }
                if (!userPaymentDetails[doc.id]){
                  userPaymentDetails[doc.id]={}; // test first
                }
                if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
                  // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                    pgmContractId:'',
                    pgmTransactions:{},
                    packageId:'', // packageId of the current contract
                    monthlyPkgId:'', // monthly pkgId
                    startDate:'', // start date of the current contract
                    endDate:'', // endDate of the current contract
                    payments:[], // payments for the contract
                    contractLength: 0, // month count
                    freezeCount:0,
                    freeMonthCount:0,
                    refMonthCount:0
                  }; // default
                }
                if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']){
                  // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']=[]; // default
                }
                contractLength++;
                freeMonthCount++;
                if (!endDate){
                  endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                  endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                if (userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] === ""){ // for empty string
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = startDate;
                }
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['freeMonthCount'] = freeMonthCount;
              }
              // temporary disable
              // else if (combinedData[indexx].source && (combinedData[indexx].source.includes('freezeTerminate'))){ // for freezeTerminate
              //   // skip?
              //   // if (startDate && contractLength === 0){ // for the next contract
              //   //   startDate = moment(x.iterationDate).format('YYYY-MM-DD');
              //   // }
              //   contractLength = 0;
              //   // contractCounter++;
              //   freeMonthCount=0;
              //   freezeCount=0;
              //   paidMonthCount=0;
              //   refMonthCount=0;
  
              // }
              else if (combinedData[indexx].source && (combinedData[indexx].source.includes('reeze'))){ // for freeze & specialFreeze
                freezeCount++;
                contractLength++;
                if (!endDate){
                  endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                  endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
                paymentsByContract.push({
                  ...x,
                  ...combinedData[indexx],
                  payType:'freeze',
                  endDate
                });
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['freezeCount'] = freezeCount;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
                 // remove the combinedData to avoid repetition
                 // combinedData.splice(indexx, 1); 
              }
  
              else if (combinedData[indexx].monthlyPkgId && (combinedData[indexx].monthlyPkgId != contractpackageID)){ 
                // for new contract
                contractLength = 0;
                contractCounter++;
                freeMonthCount=0;
                freezeCount=0;
                paidMonthCount=1;
                refMonthCount=0;
  
                if (!userPaymentDetails){
                  userPaymentDetails={}
                }
                if (!userPaymentDetails[doc.id]){
                  userPaymentDetails[doc.id]={}; // test first
                }
                if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
                  // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                    packageId:'', // packageId of the current contract
                    monthlyPkgId:'', // monthly packageId
                    startDate:'', // start date of the current contract
                    endDate:'', // endDate of the current contract
                    payments:[], // payments for the contract
                    contractLength: 0, // month count
                    freezeCount:0,
                    freeMonthCount:0,
                    refMonthCount:0,
                    paidMonthCount:1,
                    combinedTotalPrice:0
                  }; // default
                }
                if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']){
                  // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']=[]; // default
                }
                if (combinedData[indexx].totalPrice && parseFloat(combinedData[indexx].totalPrice)>0 && !combinedData[indexx].isFree){
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['combinedTotalPrice'] += parseFloat(combinedData[indexx].totalPrice);
                }
                contractLength++
                contractpackageID = combinedData[indexx].monthlyPkgId; // replace with the new packageId
                startDate = moment(x.iterationDate).format('YYYY-MM-DD');
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = startDate;
               
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = moment(x.date).format('YYYY-MM-DD'); // need to have 1 more loop?
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['packageId'] = contractpackageID;
               // reset the payments
                paymentsByContract = [{
                  ...x,
                  // date:array[indexx].date,
                  ...combinedData[indexx],
                  endDate
                }];
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
                if (!endDate){
                  endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                  endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
                 // remove the combinedData to avoid repetition
                 // combinedData.splice(indexx, 1); 
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'].push({
                //   ...x,
                //   ...combinedData[indexx]
                // })
              }
              else{
                contractLength++;
                if (!userPaymentDetails){
                  userPaymentDetails={}
                }
                if (!userPaymentDetails[doc.id]){
                  userPaymentDetails[doc.id]={}; // test first
                }
                if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
                  // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                    packageId:'', // packageId of the current contract
                    monthlyPkgId:'', // monthly packageId
                    startDate:'', // start date of the current contract
                    endDate:'', // endDate of the current contract
                    payments:[], // payments for the contract
                    contractLength: 0, // month count
                    freezeCount:0,
                    freeMonthCount:0,
                    refMonthCount:0,
                    paidMonthCount:0
                  }; // default
                }
              
                // test first
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
                paymentsByContract.push({
                  // date:x.date,
                  ...x,
                  ...combinedData[indexx],
                  test:'unknown',
                  endDate,
                  startDate
                });
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
              }
            }
            // else{
         
            // }
            // if (!userPaymentDetails[doc.id]['totalContracts']){
            //   userPaymentDetails[doc.id]['totalContracts']=0;
            // }
            // else {
            //   userPaymentDetails[doc.id]['totalContracts']=contractCounter+1;
            // }
            userPaymentDetails[doc.id]['totalContracts']=contractCounter+1;
            userPaymentDetails[doc.id]['allPayments']=combinedData;
          });


        // Object.entries(userContractDetails).forEach(([userId1, contractValues1]) => {
        //     Object.entries(contractValues1).forEach(([contractId, contractValue1]) => {

        //     })
        // });

            
            










        }
      });


  
      var transInfo = [];
  
      var userContractPaymentDetails = {};
      
      const used4 = process.memoryUsage().heapUsed / 1024 / 1024;  
      console.log(`at this point (4), The script uses approximately ${Math.round(used4 * 100) / 100} MB`);
      const time1 = moment();
      var counterLoop = 0;
      Object.entries(userContractDetails).forEach(([userId1, contractValues1]) => {
        Object.entries(contractValues1).forEach(([contractId, contractValue1]) => {
          const contractStartDate = contractValue1.contractStartDate && moment(contractValue1.contractStartDate).tz('Asia/Kuala_Lumpur').format('YYYY-MM-DD');
          Object.entries(userPaymentDetails).forEach(([userId2, contractValues2]) => {
            Object.entries(contractValues2).forEach(([contractCounter, contractValue2]) => {
              const startDate = contractValue2.startDate;

              if (moment(startDate).isSameOrAfter(moment(contractStartDate).subtract(1, 'day')) && moment(startDate).isSameOrBefore(moment(contractStartDate).add(1, 'days'))){
                 // put timer for 100, 500, 600, 1000, 2000, 3000

                if (!userContractPaymentDetails[userId2]){
                  userContractPaymentDetails[userId2]={}
                }
                if (!userContractPaymentDetails[userId2][contractId]){
                  userContractPaymentDetails[userId2][contractId]={
                    ...contractValue1,
                    ...contractValue2
                  }
                }
                else{
                  userContractPaymentDetails[userId2][contractId]={
                    ...contractValue1,
                    ...contractValue2
                  }
                }
              }
            })
          })
        })
        const used41 = process.memoryUsage().heapUsed / 1024 / 1024;  
        counterLoop++;
        if ((counterLoop === 100) || (counterLoop === 500) || (counterLoop === 600) || (counterLoop === 1000) || (counterLoop === 2000)
           || (counterLoop === 3000)
           || (counterLoop === 10000)
           || (counterLoop === 20000)
           || (counterLoop === 50000)
       ){
            const timeDiff = moment().diff(time1/100, 'seconds');
            console.log(`CounterLoop ${counterLoop}, time: ${timeDiff} & ${Math.round(used41 * 100) / 100} MB`);
            console.log('object length: ', Object.keys(userContractPaymentDetails).length);
        }
      });
  
      const used5 = process.memoryUsage().heapUsed / 1024 / 1024;  
      console.log(`at this point (5), The script uses approximately ${Math.round(used5 * 100) / 100} MB`);
      console.log('data has been sorted, userContractPaymentDetails created...');
  
      Object.entries(userContractPaymentDetails).forEach(([userId1, contractValues1]) => {
        Object.entries(contractValues1).forEach(([contractId, contractValue1]) => {
          const transactions = contractValue1.transactions;
          const paymentPlanName = contractValue1.paymentPlanName;
          const userData = contractValue1.userData;
          const email = userData && userData.email;
          const payments = contractValue1.payments;
          payments && payments.forEach(payment=>{
            const paymentId = payment.paymentId;
            const createdAt = payment.createdAt;
            const totalPrice = payment.totalPrice;
            if (contractId && totalPrice && parseInt(totalPrice)>0){
              transInfo = [
                '', 
                contractId? contractId:'',
                paymentId? paymentId:'', //external transactionId
                createdAt? getTheDateFormat(createdAt, 'YYYY-MM-DD'):'',
                totalPrice? parseFloat(totalPrice).toFixed(2):'',
                '6',
                paymentPlanName? paymentPlanName:'',
                'membership',
                'debit transaction',
                email? email:''// need to remove
    
              ];
              transSheet.push(transInfo);
            }
          });
          // transactions && transactions.forEach(trans=>{
          //   const paymentId = trans.paymentId;
          //   const createdAt = trans.createdAt;
          //   const totalPrice = trans.totalPrice;
           
          //   if (contractId && totalPrice && parseInt(totalPrice)>0){
          //     transInfo = [
          //       '', 
          //       contractId? contractId:'',
          //       paymentId? paymentId:'', //external transactionId
          //       createdAt? getTheDateFormat(createdAt, 'YYYY-MM-DD'):'',
          //       totalPrice? parseFloat(totalPrice).toFixed(2):'',
          //       '6',
          //       paymentPlanName? paymentPlanName:'',
          //       'membership',
          //       'debit transaction',
          //       email? email:''// need to remove
    
          //     ];
          //     transSheet.push(transInfo);
          //   }
          // });
        })
      });
  
      const used6 = process.memoryUsage().heapUsed / 1024 / 1024;  
      console.log(`at this point (6), The script uses approximately ${Math.round(used6 * 100) / 100} MB`);
      console.log('data has been sorted, userContractPaymentDetails created...');
  
      const updateSheetPromise = updateGoogleSheet({
        spreadsheetId: pgmTransactionId,
        resource: {
          // How the input data should be interpreted.
          valueInputOption: 'RAW',  // TODO: Update placeholder value.
          // The new values to apply to the spreadsheet.
          data: [
            {
              range: `Migration Template!A2:CM`,
              majorDimension: "ROWS",
              values: transSheet
            }
          ],
        },
      });

      return updateSheetPromise.then((result)=>{
        return res.status(200).send({
   
            success:true,
            // message: `Hello ${userEmail}`,
            // email:userEmail,
            // contentType: req.get('content-type'),
            // itemData:itemData,
            heapMemory:used2,
            used3:used3,
            used4,used4,
            used5:used5,
            used6:used6,
            // userContractPaymentDetails
            // pgmContractMapByUserId,
            // transactionInfoArray
        });
        });

    });
});


functions.http('addalltransactionstopgmsheetsv2', (req, res)=>{

    const itemData = req.body;
    const userEmail = itemData && itemData.email;
    const userId = itemData && itemData.userId;
    const userQuery = userEmail? admin.firestore().collection('users').where('email', '==', userEmail).get():admin.firestore().collection('users').get();
    // const packageQuery = admin.firestore().collection('packages').get();
    const paymentQuery = userId? admin.firestore().collection('payments').where('type', '==', 'membership').
        where('userId', '==', userId).get():
        admin.firestore().collection('payments').where('type', '==', 'membership')
        // .limit(100)
        // .where('status', '==', 'CLOSED')
        .get();
    const monthlyPackageQuery = admin.firestore().collection('pgmPackages').get();
    const pgmUserQuery = admin.firestore().collection('pgmUsers').where('userType', '==', 'Member').get();
    const pgmContractQuery = admin.firestore().collection('pgmContracts').get();

    // return res.status(200).send({success:true});

    const used1 = process.memoryUsage().heapUsed / 1024 / 1024;  
    console.log(`at this point (start), The script uses approximately ${Math.round(used1 * 100) / 100} MB`);

  return Promise.all([userQuery, 
    paymentQuery, monthlyPackageQuery, 
    pgmUserQuery, pgmContractQuery
  ]).then(results=>{
    const used2 = process.memoryUsage().heapUsed / 1024 / 1024;  
    console.log(`at this point (2), The script uses approximately ${Math.round(used2 * 100) / 100} MB`);

    const userRes = results[0];
    // const pkgRes = results[1];
    const paymentRes = results[1];
    const monthlyPkgRes = results[2];
    const pgmUserRes = results[3];
    const pgmContractRes = results[4];

    var staffMap = {};
    var userSheets = [];
    var transSheet = [];
    var pkgMap = {};
    var paymentMap = {};
    var paymentArray = [];

    var pgmUserMapByFBUserId = {};
    var pgmUserMapByPGMUserId = {};
    pgmUserRes.forEach(doc=>{
      const data = doc.data();
      const fbUserId = data.fbUserId;
      const userId = data.userId;
      if (fbUserId){
        pgmUserMapByFBUserId[fbUserId] = data;
        pgmUserMapByPGMUserId[userId] = data;
      }
    });

    // pgmContractMap by pgm userId
    var pgmContractMapByUserId = {};
    var pgmContractArray = [];
    var pgmContractMap = {};
    pgmContractRes.forEach(doc=>{
      const data = doc.data();
      pgmContractMap[doc.id]=data;
      const pgmUserId = data.userId;
      const pgmUserData = pgmUserId && pgmUserMapByPGMUserId[pgmUserId];
      const pgmFBUserId = pgmUserData && pgmUserData.fbUserId;
      const userHomeClubId = data.userHomeClubId;
      const contractId = data.contractId;

      if (pgmUserId && userHomeClubId && (userHomeClubId === 1||userHomeClubId === 2)){
        pgmContractArray = pgmContractMapByUserId[pgmUserId]||[];
        pgmContractArray.push(data);
        pgmContractMapByUserId[pgmUserId]=pgmContractArray;
      }
      // pgmContractMapByUserId[doc.id]=data;
    });

    var monthlyPkgMap = {}
    var monthlypkgMapByPrice = {};
    var monthlyPkgArray = [];
    monthlyPkgRes.forEach(doc=>{
      const data = doc.data();
      const packageIds = data.packageIds;
      const price = data.price && parseFloat(data.price).toFixed(2);
      monthlypkgMapByPrice[price]=data;
      monthlypkgMapByPrice[price].id = doc.id;
      monthlyPkgMap[doc.id]=data;
      // packageIds && packageIds.forEach(packageId=>{
      //   monthlyPkgMap[]
      // });
    });

    var paymentMapByContractId = {};
    var paymentArrayByContract = [];
    // var transactionInfoArray = [];
    var transactionInfoMap = {};
  
    paymentRes.forEach(doc=>{
        const data = doc.data();
        const userId = data.userId;
        const status = data.status;
        const freezeFor = data.freezeFor;
        const source = data.source;
        const type = data.type;
        const pgmUserData = userId && pgmUserMapByFBUserId[userId];
        const pgmUserId = pgmUserData && pgmUserData.userId;
        const pgmContractData = pgmUserId && pgmContractMapByUserId[pgmUserId];
  
        if (((type && type==='membership') && (status && status === 'CLOSED')) 
            && pgmContractData 
            // || (freezeFor) 
            || (source && (source === 'join' || source === 'luckyDraw' || source === 'promo' || source === 'free' || source === 'complimentary' || source === 'jfr' || source === 'refer'))
          ){
          paymentArray = paymentMap[userId] || [];
          paymentArray.push({...data, paymentId:doc.id});
          paymentMap[userId]=paymentArray;
        }
      });

      var userPaymentDetails = {};
      var userContractDetails = {};
  
      const used3 = process.memoryUsage().heapUsed / 1024 / 1024;  
      console.log(`at this point (3), The script uses approximately ${Math.round(used3 * 100) / 100} MB`);

    
    //   userRes.forEach(doc=>{
    //     const data = doc.data();
    //     const pgmUserData = (doc.id && pgmUserMapByFBUserId[doc.id]);
    //     const pgmUserId = pgmUserData && pgmUserData.userId;

    //     const paymentData = paymentMap[doc.id];
      
    //     // get pgmcontract for each user
    //     const pgmUserContractDataArray = pgmUserId && pgmContractMapByUserId[pgmUserId];

    //     if (pgmUserContractDataArray && pgmUserContractDataArray.length>0 && paymentData && paymentData.length>0 && pgmUserId){
    //         paymentData.forEach(payData=>{
    //             const userId = payData.userId;
                
    //         });

    //         pgmUserContractDataArray.forEach(pgmContractData=>{
    //             transactionInfoArray.push(pgmContractData);
    //         });
    //     }
    //   });

      userRes && userRes.forEach(doc=>{
        const data = doc.data();
        const packageId = data && data.packageId;
        const email = data && data.email;
        const name = data && data.name;
        const lastName = getLastName(data);
        // const gender = data && data.gender;
        const firstJoinVisit = data && data.firstJoinVisit;
        const clubId = firstJoinVisit? (firstJoinVisit === 'TTDI')? 101 : (firstJoinVisit === 'KLCC')? 102 : 101 : 101;
        // const gantnerCardNumber = data && data.gantnerCardNumber;
        const membershipEnds = data && getMembershipEnd(data);
        const membershipStarts = data && getMembershipStart(data);
        const isActive = packageId && membershipEnds && membershipStarts && moment(getTheDate(membershipEnds)).isSameOrAfter(moment());
        const isGuest = !membershipEnds && !membershipStarts;
        const isMember = (packageId && membershipEnds && membershipStarts)? true:false;
        // const automaticRenew = isActive? 1:0;
        // const mcId = data && data.mcId;
        // const consultantData = mcId && staffMap[mcId];
        // const consultantEmail = consultantData && consultantData.email;
        const remarks = data && data.remarks;
        const isStaff = data && data.isStaff;
        const roles = data && data.roles;
        const staffRole = data && data.staffRole;
        const paymentData = paymentMap[doc.id];
        const isTestEmail = email && (email.toLowerCase().includes('faizul') || email.toLowerCase().includes('billy'));
        const pgmUserData = pgmUserMapByFBUserId[doc.id];
        const pgmUserId = pgmUserData && pgmUserData.userId;
        // const pgmUserContractsData = pgmUserData && pgmUserData.userId && pgmContractMapByUserId[pgmUserData.userId];
        const pgmUserContractsData = pgmUserId && pgmContractMapByUserId[pgmUserId];
  
        // not inclde duplicate name
        if (isMember 
          && name 
          && !isStaff
          && !roles
          && !staffRole
          && !isGuest
          && paymentData
          && !isTestEmail
          && pgmUserContractsData
          // && (email && email === 'liliankohlb@yahoo.com' || email === 'j.panterbrick@gmail.com' || email === 'aishaashikin.yaacob@gmail.com')
        ){
          
           // sort the payment
           paymentData && paymentData.sort((a,b)=>{
            const createdA = a.createdAt;
            const createdB = b.createdAt;
            if(createdA < createdB){return 1}
            else if(createdB < createdA){return -1;}
            else{return 0}
          });
          paymentData && paymentData.reverse();
  
          var monthStartCount = 0;
          var monthEndCount = 1;
          var startMoment = moment(getTheDate(membershipStarts)).tz('Asia/Kuala_Lumpur').startOf('day');
          var endMoment = moment(getTheDate(membershipEnds)).tz('Asia/Kuala_Lumpur').startOf('day');
  
          var userFreezes = [];
          var userFreezeTerminated = [];
          var userFreeAccess = [];
          var membershipHistoryList = [];
          var combinedData = [];
          var combinedVendMth = [];
          var combinedTransactions = [];
          var addMonths = 0;
          var addYears = 0;
          
          var paymentCount = 0;
          paymentData && paymentData.forEach((payData, index)=>{
            const freezeFor = payData.freezeFor;
            const packageId = payData.packageId;
            const paymentCreatedDate = payData.createdAt;
            const renewalTerm = (payData && payData.renewalTerm) || 'month';
            const source = payData.source;
            const qty = payData.quantity||1;
            const cardSummary = payData.cardSummary;
            const cardExpired = payData.cardExpiryDate;
            const price = payData.totalPrice? payData.totalPrice:0;
            const status = payData.status;
            const type = payData.type;
            const paymentType = payData.paymentType;
            const userId = payData.userId;
            const paymentId = payData.paymentId;
         
            // 1. need to sort the freeze first
            if (source && freezeFor){
              const freezeType = payData.freezeType;
              for (var a = 0; a<qty; a++){
                userFreezes.push({
                  date:moment(getTheDate(freezeFor)).add(a, 'months'),
                  freezeType,
                  yearOfFreeze: moment(freezeFor).format('YYYY'),
                  cardSummary, cardExpired, paymentId,
                  ...payData,
                  isFreeze:true
                  // need to link with contract?
                });
              }
            }
            else if (source==='freezeTerminate'){
              userFreezeTerminated.push({date:moment(freezeFor), paymentId});
            }
            else if ((source==='join') || (source==='luckyDraw') || (source==='promo')|| (source==='free')
            || (source==='complimentary') || (source==='jfr') || (source==='refer') 
            || (parseInt(price)===0)
            ){
              userFreeAccess.push({
                date:moment(getTheDate(paymentCreatedDate)), type:source, paymentId, 
                ...payData,
                isFree:true
              });
            }
  
            else if (((source === 'vend') || ((source === 'adyen') && (parseInt(price)!=0)) || (source==='pbonline'))
            && (status === 'CLOSED') && (type === 'membership')){
              for (var h=0; h<qty; h++){
                combinedTransactions.push({
                  ...payData,
                  paymentId,
                  cycle:h+1
                })
                // combinedVendMth.push({
                //   date:moment(paymentCreatedDate).add(i, 'months'), 
                //   paymentDate:paymentCreatedDate,
                //   type:source,
                //   price, paymentType, cardSummary, cardExpired,
                //   packageId, paymentId, renewalTerm, 
                //   ...payData
                // });
              }
              // for the combined transactions
             
              var monthlyPkgPrice = 0;
              var monthlyPkgData;
              if (renewalTerm && (renewalTerm === 'year' || renewalTerm === 'yearly')){
                monthlyPkgPrice = price && parseFloat(price/12*qty).toFixed(2);
                monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
                const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
                const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
      
                for (var i=0; i<(qty*12); i++){
                  combinedVendMth.push({
                    date:moment(getTheDate(paymentCreatedDate)).add(i, 'months'), 
                    paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                    type:source,
                    cycle:i+1,
                    ...payData,
                    ...monthlyPkgData,
                    monthlyPkgId,
                    monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                    // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                  });
                  // if (cardSummary){
                  //   combinedVendMth[i].cardSummary = `${cardSummary} ${cardExpired}`;
                  // }
                }
              }
              else if (renewalTerm && (renewalTerm === 'month' || renewalTerm === 'monthly')){
                monthlyPkgPrice = price && parseFloat(price/qty).toFixed(2);
                monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
                const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
                const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
                for (var j=0; j<qty; j++){
                  combinedVendMth.push({
                    date:moment(getTheDate(paymentCreatedDate)).add(j, 'months'),
                    paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                    type:source,
                    cycle:j+1,
                    ...payData,
                    ...monthlyPkgData,
                    monthlyPkgId,
                    monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                    // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=220)? true:false
                    isFree: (price && parseInt(price) === 0)? true:false
                  });
                  // if (cardSummary){
                  //   combinedVendMth[j].cardSummary = `${cardSummary} ${cardExpired}`;
                  // }
                }
              }
              else if (renewalTerm && (renewalTerm === 'biyearly' || renewalTerm === 'biyear')){
                monthlyPkgPrice = price && parseFloat(price/6*qty).toFixed(2);
                monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
                const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
                const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
                for (var k=0; k<qty*6; k++){
                  combinedVendMth.push({
                    date:moment(getTheDate(paymentCreatedDate)).add(k, 'months'), 
                    paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                    cycle:k+1,
                    ...payData,
                    ...monthlyPkgData,
                    monthlyPkgId,
                    monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                    // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                  });
                  // if (cardSummary){
                  //   combinedVendMth[k].cardSummary = `${cardSummary} ${cardExpired}`;
                  // }
                }
              }
              else if (renewalTerm && renewalTerm === 'quarterly'){
                monthlyPkgPrice = price && parseFloat(price/3*qty).toFixed(2);
                monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
                const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
                const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
                for (var l=0; l<qty*3; l++){
                  combinedVendMth.push({
                    date:moment(getTheDate(paymentCreatedDate)).add(l, 'months'), 
                    paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                    cycle:l+1,
                    ...payData,
                    ...monthlyPkgData,
                    monthlyPkgId,
                    monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                    // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                  });
                  // if (cardSummary){
                  //   combinedVendMth[l].cardSummary = `${cardSummary} ${cardExpired}`;
                  // }
                }
              }
              else if (renewalTerm && renewalTerm === '4monthly'){
                monthlyPkgPrice = price && parseFloat(price/4*qty).toFixed(2);
                monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
                const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
                const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
                for (var m=0; m<qty*4; m++){
                  combinedVendMth.push({
                    date:moment(getTheDate(paymentCreatedDate)).add(m, 'months'), 
                    paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                    cycle:m+1,
                    ...payData,
                    ...monthlyPkgData,
                    monthlyPkgId,
                    monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                    // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                  });
                  // if (cardSummary){
                  //   combinedVendMth[m].cardSummary = `${cardSummary} ${cardExpired}`;
                  // }
                }
              }
              // for complimentary
              else if (renewalTerm && renewalTerm === 'never'){
  
              }
            }
  
            // resort the array dates
            userFreezes.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
            userFreezes.reverse();
            userFreezeTerminated.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
            userFreezeTerminated.reverse();
            userFreeAccess.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
            userFreeAccess.reverse();
            combinedVendMth.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
            combinedVendMth.reverse();
  
            const initialMonthsDiff = Math.max(moment(new Date()).diff(startMoment, 'months'));
            var monthsDiff = initialMonthsDiff;
            var totalArrayLength = userFreezeTerminated.length + userFreezes.length + userFreeAccess.length + combinedVendMth.length;
            if (totalArrayLength>initialMonthsDiff){
              monthsDiff = totalArrayLength-1;
            }
  
            // default, if there is no payment detected
            for (var i=0; i<=monthsDiff; i++){
              const iterationStartMoment = startMoment.clone().add(i, 'months');
              // combinedItems.push({effectiveDate:iterationStartMoment, primaryText:primaryText, secondaryText: secondaryText, action:action});
              membershipHistoryList.push({
                iterationDate:iterationStartMoment.format('YYYY-MM-DD'), 
                count:i,
                userId
                // addMonths, addYears,
                // startMoment:startMoment && startMoment.format('YYYY-MM-DD')
              });
            }
  
            membershipHistoryList && membershipHistoryList.forEach((x,indexx)=>{
              if (userFreezeTerminated && userFreezeTerminated.length>0 
                && moment(x.iterationDate).isSameOrAfter(userFreezeTerminated[userFreezeTerminated.length-1].date)
                // && moment(userFreezeTerminated[userFreezeTerminated.length-1].date).isBetween(x.date, moment(x.date).add(1,'month'))
                // && moment(x.date).isBetween(userFreezeTerminated[userFreezeTerminated.length-1].date, userFreezeTerminated[userFreezeTerminated.length-1].date.add('months', 1))
              ){
                  combinedData.push({
                    date:userFreezeTerminated[userFreezeTerminated.length-1].date,
                    type:'freezeTerminated',
                    index:indexx,
                    ...userFreezeTerminated[userFreezeTerminated.length-1]
                  });
                  userFreezeTerminated.pop();
              }
              else if (userFreezes && userFreezes.length>0
                && moment(x.iterationDate).isSameOrAfter(userFreezes[userFreezes.length-1].date.clone()) 
                && moment(x.iterationDate).isBefore(userFreezes[userFreezes.length-1].date.clone().add(1, 'months')) 
                // && moment(userFreezes[userFreezes.length-1].date).isBetween(x.date, (x.date).add('month', 1).subtract('days', 1))
                // && moment(x.date).isBetween(userFreezes[userFreezes.length-1].date, userFreezes[userFreezes.length-1].date.add('months', 1))
                ){
                combinedData.push({
                  // date:userFreezes[userFreezes.length-1].date,
                  // type:'freeze',
                  // freezeType: userFreezes[userFreezes.length-1].freezeType? userFreezes[userFreezes.length-1].freezeType:null,
                  // price:userFreezes[userFreezes.length-1].price, 
                  // freezeCountPerYear:userFreezes[userFreezes.length-1].freezeCountPerYear,
                  index:indexx,
                  // paymentId:userFreezes[userFreezes.length-1].paymentId,
                  ...userFreezes[userFreezes.length-1]
                });
                userFreezes.pop();
              }
              else if (userFreeAccess && userFreeAccess.length>0 
                // && userFreeAccess[userFreeAccess.length-1].date.isSameOrAfter(moment(x.date))
                && moment(x.iterationDate).isSameOrAfter(userFreeAccess[userFreeAccess.length-1].date)
                // && userFreeAccess[userFreeAccess.length-1].date.isBetween(x.date, moment(x.date).add(1,'month'))
                ){
                combinedData.push({
                  // date:userFreeAccess[userFreeAccess.length-1].date,
                  // type:userFreeAccess[userFreeAccess.length-1].type,
                  index:indexx,
                //  paymentId:userFreeAccess[userFreeAccess.length-1].paymentId,
                 ...userFreeAccess[userFreeAccess.length-1]
                })
                userFreeAccess.pop();   
              }
              else if (combinedVendMth && combinedVendMth.length>0){
                combinedData.push({
                  // date:combinedVendMth[combinedVendMth.length-1].date,
                  // paymentDate: combinedVendMth[combinedVendMth.length-1].paymentDate,
                  // type:combinedVendMth[combinedVendMth.length-1].type,
                  // // visitLeft:combinedVendMth[combinedVendMth.length-1].visitLeft,
                  // // visitMax: combinedVendMth[combinedVendMth.length-1].visitMax,
                  // price:combinedVendMth[combinedVendMth.length-1].price, 
                  // paymentType: combinedVendMth[combinedVendMth.length-1].paymentType,
                  // cardSummary: combinedVendMth[combinedVendMth.length-1].cardSummary,
                  // cardExpired: combinedVendMth[combinedVendMth.length-1].cardExpired,
                  // index:indexx,
                  // packageId:combinedVendMth[combinedVendMth.length-1].packageId,
                  // paymentId:combinedVendMth[combinedVendMth.length-1].paymentId,
                  // renewalTerm: combinedVendMth[combinedVendMth.length-1].renewalTerm,
                  index:indexx,
                  ...combinedVendMth[combinedVendMth.length-1]
                })
                combinedVendMth.pop();
              }
             
              else{
                // combinedData.push({
                //   ...x,
                //   test: 'outside of loop',
                //   iterationDate:x.iterationDate
                // })
              }
            });
  
            // sorting...
            combinedData.sort((a,b) => {
              const totalPriceA = a.totalPrice? parseInt(a.totalPrice):0;
              const totalPriceB = b.totalPrice? parseInt(a.totalPrice):0;
              const dateA = a.date.format('YYYY-MM-DD');
              const dateB = b.date.format('YYYY-MM-DD');
              if (dateA>dateB){
                return 1
                // if (totalPriceA>totalPriceB){
                //   return 1;
                // }
                // else if (totalPriceA<totalPriceB){
                //   return -1;
                // }
                // else{
                //   return 1
                // }
              }
              else if (dateA<dateB){
                return -1;
                // if (totalPriceA>totalPriceB){
                //   return -1;
                // }
                // else if (totalPriceA<totalPriceB){
                //   return 1;
                // }
                // else{
                //   return -1
                // }
              }
              else if (dateA === dateB){
                if (totalPriceA>totalPriceB){
                  return 1;
                }
                else if (totalPriceA<totalPriceB){
                  return -1;
                }
                else{
                  return 0
                }
              }
              else {
                return 0
              }
  
              // if (dateA === dateB)
              // a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD')
              // const createdA = a.createdAt;
              // const createdB = b.createdAt;
              // if(createdA < createdB){return 1}
              // else if(createdB < createdA){return -1;}
              // else{return 0}
            });
            combinedTransactions.sort((a,b)=>a.createdAt - b.createdAt);
  
          });
          
          var contractLength = 0;
          var refMonthCount = 0;
          var paidMonthCount = 0;
          var paymentsByContract = [];
          var contractpackageID = null;
          var contractCounter = 0;
          var freezeCount = 0;
          var freeMonthCount = 0;
          var startDate;
          var endDate;
          var transactionsByContract=[];
          var combinedTotalPrice = 0;
  
          pgmUserContractsData.forEach(pgmContracts=>{
            const contractId=pgmContracts.contractId;
            const contractStartDate = pgmContracts.contractStartDate;
            const contractEndDate = pgmContracts.contractEndDate;
  
            if (contractId){
              // if (!userPaymentDetails[doc.id]){
              //   userPaymentDetails[doc.id]={};
              // }
              // if (!userPaymentDetails[doc.id][contractId]){
              //   userPaymentDetails[doc.id][contractId]=
              //   {
              //     ...pgmContracts,
              //   }
              // }
              if (!userContractDetails[doc.id]){
                userContractDetails[doc.id]={};
              }
              if (!userContractDetails[doc.id][contractId]){
                userContractDetails[doc.id][contractId]=
                {
                  ...pgmContracts,
                  monthlyPkgId:contractpackageID,
                  packageId:'', // packageId of the current contract
                  startDate:startDate, // start date of the current contract
                  endDate:endDate, // endDate of the current contract
                  payments:[], // payments for the contract
                  // contractLength: 0, // month count
                  freezeCount:0,
                  freeMonthCount:0,
                  paidMonthCount:0,
                  refMonthCount:0,
                  combinedTotalPrice:0,
                }
              }
              userContractDetails[doc.id][contractId]={
                ...pgmContracts,
                monthlyPkgId:contractpackageID,
                packageId:'', // packageId of the current contract
                startDate:startDate, // start date of the current contract
                endDate:endDate, // endDate of the current contract
                payments:[], // payments for the contract
                // contractLength: 0, // month count
                freezeCount:0,
                freeMonthCount:0,
                paidMonthCount:0,
                refMonthCount:0,
                combinedTotalPrice:0,
              }
            }
          });
  
          membershipHistoryList.forEach((x, indexx, array)=>{
            // // compare with contract id here...
            // Object.entries(userContractDetails).forEach(([userId1, contractValues1]) => {
            //     Object.entries(contractValues1).forEach(([contractId, contractValue1]) => {

            //     });
            // });

            if (!userPaymentDetails){
              userPaymentDetails={}
            }
            if (!userPaymentDetails[doc.id]){
              userPaymentDetails[doc.id]={
                totalContracts:0
              }; // test first
            }
            if (!userPaymentDetails[doc.id]['totalContracts']){
              userPaymentDetails[doc.id]['totalContracts'] = 0;
            }
          
            if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
              // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                packageId:'', // packageId of the current contract
                monthlyPkgId:'',
                startDate:'', // start date of the current contract
                endDate:'', // endDate of the current contract
                payments:[], // payments for the contract
                contractLength: 0, // month count
                freezeCount:0,
                freeMonthCount:0,
                paidMonthCount:0,
                refMonthCount:0,
                combinedTotalPrice:0,
                // monthlyPkgPrice:0
              }; // default
            }
            if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']){
              // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']=[]; // default
            }
  
            userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['userId'] = x.userId;
            userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['userData'] = data;
            userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['userData']['lastName'] = lastName;
  
            if (!startDate){ // default
              startDate = moment(x.iterationDate).format('YYYY-MM-DD');
            }
  
            if (combinedData && combinedData.length>0 && indexx<combinedData.length){
              if (combinedData[indexx].totalPrice && parseFloat(combinedData[indexx].totalPrice)>0 && !combinedData[indexx].isFree){
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['combinedTotalPrice'] += parseFloat(combinedData[indexx].totalPrice);
              }
  
              if (combinedData[indexx].monthlyPkgId && (!contractpackageID || (contractpackageID===combinedData[indexx].monthlyPkgId))
                && (!combinedData[indexx].isFree) && (!(combinedData[indexx].type && combinedData[indexx].type === 'freezeTerminated'))
                ){
                if (!contractpackageID){
                  contractpackageID = combinedData[indexx].monthlyPkgId;
                  startDate = startMoment.format('YYYY-MM-DD');
                }
                if ((startDate && contractLength === 0) || !startDate){ // for the next contract
                  startDate = moment(x.iterationDate).format('YYYY-MM-DD');
                }
                if (!endDate){
                  endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                  endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                
                // endDate = moment(startDate).add(contractLength+1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                // if (moment(x.iterationDate).isAfter(startDate) && ){
                //   startDate = x.date;
                // }
                contractLength++;
                paidMonthCount++;
                
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = startDate;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['packageId'] = contractpackageID;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['monthlyPkgId'] = contractpackageID;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['paidMonthCount'] = paidMonthCount;
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`][`monthlyPkgPrice`]
                paymentsByContract.push({
                  // date:array[indexx].date,
                  ...x,
                  ...combinedData[indexx],
                  payType:'paid',
                  paidMonthCount,
                  endDate
                });
                transactionsByContract.push({
                  ...combinedTransactions[indexx]
                });
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['transactions'] = transactionsByContract;
                // remove the combinedData to avoid repetition
                // combinedData.splice(indexx, 1); 
              }
              // for referral
              else if(!combinedData[indexx].monthlyPkgId && combinedData[indexx].source && combinedData[indexx].source === 'refer'){
                refMonthCount++;
                contractLength++
                paymentsByContract.push({
                  ...x,
                  ...combinedData[indexx],
                  payType:'refer',
                  endDate
                });
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['refMonthCount'] = refMonthCount;
                if (!endDate){
                  endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                  endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
              }
              // for free
              else if ((combinedData[indexx].monthlyPkgId && combinedData[indexx].totalPrice && (parseInt(combinedData[indexx].totalPrice)===0 && contractpackageID && combinedData[indexx].isFree))
                || (combinedData[indexx].totalPrice && parseInt(combinedData[indexx].totalPrice)===0 && combinedData[indexx].source && combinedData[indexx].source != 'freeze')
                || (contractpackageID && combinedData[indexx].isFree)
              ){ // for free, use the current contract packageId
                paymentsByContract.push({
                  ...x,
                  ...combinedData[indexx],
                  payType:'free',
                  startDate,
                  endDate
                });
                if (!userPaymentDetails){
                  userPaymentDetails={}
                }
                if (!userPaymentDetails[doc.id]){
                  userPaymentDetails[doc.id]={}; // test first
                }
                if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
                  // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                    pgmContractId:'',
                    pgmTransactions:{},
                    packageId:'', // packageId of the current contract
                    monthlyPkgId:'', // monthly pkgId
                    startDate:'', // start date of the current contract
                    endDate:'', // endDate of the current contract
                    payments:[], // payments for the contract
                    contractLength: 0, // month count
                    freezeCount:0,
                    freeMonthCount:0,
                    refMonthCount:0
                  }; // default
                }
                if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']){
                  // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']=[]; // default
                }
                contractLength++;
                freeMonthCount++;
                if (!endDate){
                  endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                  endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                if (userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] === ""){ // for empty string
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = startDate;
                }
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['freeMonthCount'] = freeMonthCount;
              }
              // temporary disable
              // else if (combinedData[indexx].source && (combinedData[indexx].source.includes('freezeTerminate'))){ // for freezeTerminate
              //   // skip?
              //   // if (startDate && contractLength === 0){ // for the next contract
              //   //   startDate = moment(x.iterationDate).format('YYYY-MM-DD');
              //   // }
              //   contractLength = 0;
              //   // contractCounter++;
              //   freeMonthCount=0;
              //   freezeCount=0;
              //   paidMonthCount=0;
              //   refMonthCount=0;
  
              // }
              else if (combinedData[indexx].source && (combinedData[indexx].source.includes('reeze'))){ // for freeze & specialFreeze
                freezeCount++;
                contractLength++;
                if (!endDate){
                  endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                  endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
                paymentsByContract.push({
                  ...x,
                  ...combinedData[indexx],
                  payType:'freeze',
                  endDate
                });
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['freezeCount'] = freezeCount;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
                 // remove the combinedData to avoid repetition
                 // combinedData.splice(indexx, 1); 
              }
  
              else if (combinedData[indexx].monthlyPkgId && (combinedData[indexx].monthlyPkgId != contractpackageID)){ 
                // for new contract
                contractLength = 0;
                contractCounter++;
                freeMonthCount=0;
                freezeCount=0;
                paidMonthCount=1;
                refMonthCount=0;
  
                if (!userPaymentDetails){
                  userPaymentDetails={}
                }
                if (!userPaymentDetails[doc.id]){
                  userPaymentDetails[doc.id]={}; // test first
                }
                if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
                  // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                    packageId:'', // packageId of the current contract
                    monthlyPkgId:'', // monthly packageId
                    startDate:'', // start date of the current contract
                    endDate:'', // endDate of the current contract
                    payments:[], // payments for the contract
                    contractLength: 0, // month count
                    freezeCount:0,
                    freeMonthCount:0,
                    refMonthCount:0,
                    paidMonthCount:1,
                    combinedTotalPrice:0
                  }; // default
                }
                if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']){
                  // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']=[]; // default
                }
                if (combinedData[indexx].totalPrice && parseFloat(combinedData[indexx].totalPrice)>0 && !combinedData[indexx].isFree){
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['combinedTotalPrice'] += parseFloat(combinedData[indexx].totalPrice);
                }
                contractLength++
                contractpackageID = combinedData[indexx].monthlyPkgId; // replace with the new packageId
                startDate = moment(x.iterationDate).format('YYYY-MM-DD');
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = startDate;
               
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = moment(x.date).format('YYYY-MM-DD'); // need to have 1 more loop?
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['packageId'] = contractpackageID;
               // reset the payments
                paymentsByContract = [{
                  ...x,
                  // date:array[indexx].date,
                  ...combinedData[indexx],
                  endDate
                }];
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
                if (!endDate){
                  endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                  endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
                }
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
                 // remove the combinedData to avoid repetition
                 // combinedData.splice(indexx, 1); 
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'].push({
                //   ...x,
                //   ...combinedData[indexx]
                // })
              }
              else{
                contractLength++;
                if (!userPaymentDetails){
                  userPaymentDetails={}
                }
                if (!userPaymentDetails[doc.id]){
                  userPaymentDetails[doc.id]={}; // test first
                }
                if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
                  // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                  userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                    packageId:'', // packageId of the current contract
                    monthlyPkgId:'', // monthly packageId
                    startDate:'', // start date of the current contract
                    endDate:'', // endDate of the current contract
                    payments:[], // payments for the contract
                    contractLength: 0, // month count
                    freezeCount:0,
                    freeMonthCount:0,
                    refMonthCount:0,
                    paidMonthCount:0
                  }; // default
                }
              
                // test first
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
                paymentsByContract.push({
                  // date:x.date,
                  ...x,
                  ...combinedData[indexx],
                  test:'unknown',
                  endDate,
                  startDate
                });
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
              }
            }
            // else{
         
            // }
            // if (!userPaymentDetails[doc.id]['totalContracts']){
            //   userPaymentDetails[doc.id]['totalContracts']=0;
            // }
            // else {
            //   userPaymentDetails[doc.id]['totalContracts']=contractCounter+1;
            // }
            userPaymentDetails[doc.id]['totalContracts']=contractCounter+1;
            userPaymentDetails[doc.id]['allPayments']=combinedData;
          });


        // Object.entries(userContractDetails).forEach(([userId1, contractValues1]) => {
        //     Object.entries(contractValues1).forEach(([contractId, contractValue1]) => {

        //     })
        // });

            
            










        }
      });


  
      var transInfo = [];
  
      var userContractPaymentDetails = {};
      
      const used4 = process.memoryUsage().heapUsed / 1024 / 1024;  
      console.log(`at this point (4), The script uses approximately ${Math.round(used4 * 100) / 100} MB`);
      const time1 = moment();
      var counterLoop = 0;
      Object.entries(userContractDetails).forEach(([userId1, contractValues1]) => {
        Object.entries(contractValues1).forEach(([contractId, contractValue1]) => {
          const contractStartDate = contractValue1.contractStartDate && moment(contractValue1.contractStartDate).tz('Asia/Kuala_Lumpur').format('YYYY-MM-DD');
          Object.entries(userPaymentDetails).forEach(([userId2, contractValues2]) => {
            Object.entries(contractValues2).forEach(([contractCounter, contractValue2]) => {
              const startDate = contractValue2.startDate;

              if (moment(startDate).isSameOrAfter(moment(contractStartDate).subtract(1, 'day')) && moment(startDate).isSameOrBefore(moment(contractStartDate).add(1, 'days'))){
                 // put timer for 100, 500, 600, 1000, 2000, 3000

                if (!userContractPaymentDetails[userId2]){
                  userContractPaymentDetails[userId2]={}
                }
                if (!userContractPaymentDetails[userId2][contractId]){
                  userContractPaymentDetails[userId2][contractId]={
                    ...contractValue1,
                    ...contractValue2
                  }
                }
                else{
                  userContractPaymentDetails[userId2][contractId]={
                    ...contractValue1,
                    ...contractValue2
                  }
                }
              }
            })
          })
        })
        const used41 = process.memoryUsage().heapUsed / 1024 / 1024;  
        counterLoop++;
        if ((counterLoop === 100) || (counterLoop === 500) || (counterLoop === 600) || (counterLoop === 1000) || (counterLoop === 2000)
           || (counterLoop === 3000)
           || (counterLoop === 10000)
           || (counterLoop === 20000)
           || (counterLoop === 50000)
       ){
            const timeDiff = moment().diff(time1/100, 'seconds');
            console.log(`CounterLoop ${counterLoop}, time: ${timeDiff} & ${Math.round(used41 * 100) / 100} MB`);
            console.log('object length: ', Object.keys(userContractPaymentDetails).length);
        }
        
      });
  
      const used5 = process.memoryUsage().heapUsed / 1024 / 1024;  
      console.log(`at this point (5), The script uses approximately ${Math.round(used5 * 100) / 100} MB`);
      console.log('data has been sorted, userContractPaymentDetails created...');
  
      Object.entries(userContractPaymentDetails).forEach(([userId1, contractValues1]) => {
        Object.entries(contractValues1).forEach(([contractId, contractValue1]) => {
          const transactions = contractValue1.transactions;
          const paymentPlanName = contractValue1.paymentPlanName;
          const userData = contractValue1.userData;
          const email = userData && userData.email;
          const payments = contractValue1.payments;
          payments && payments.forEach(payment=>{
            const paymentId = payment.paymentId;
            const createdAt = payment.createdAt;
            const totalPrice = payment.totalPrice;
            if (contractId && totalPrice && parseInt(totalPrice)>0){
              transInfo = [
                '', 
                contractId? contractId:'',
                paymentId? paymentId:'', //external transactionId
                createdAt? getTheDateFormat(createdAt, 'YYYY-MM-DD'):'',
                totalPrice? parseFloat(totalPrice).toFixed(2):'',
                '6',
                paymentPlanName? paymentPlanName:'',
                'membership',
                'debit transaction',
                email? email:''// need to remove
    
              ];
              transSheet.push(transInfo);
            }
          });
          // transactions && transactions.forEach(trans=>{
          //   const paymentId = trans.paymentId;
          //   const createdAt = trans.createdAt;
          //   const totalPrice = trans.totalPrice;
           
          //   if (contractId && totalPrice && parseInt(totalPrice)>0){
          //     transInfo = [
          //       '', 
          //       contractId? contractId:'',
          //       paymentId? paymentId:'', //external transactionId
          //       createdAt? getTheDateFormat(createdAt, 'YYYY-MM-DD'):'',
          //       totalPrice? parseFloat(totalPrice).toFixed(2):'',
          //       '6',
          //       paymentPlanName? paymentPlanName:'',
          //       'membership',
          //       'debit transaction',
          //       email? email:''// need to remove
    
          //     ];
          //     transSheet.push(transInfo);
          //   }
          // });
        })
      });
  
      const used6 = process.memoryUsage().heapUsed / 1024 / 1024;  
      console.log(`at this point (6), The script uses approximately ${Math.round(used6 * 100) / 100} MB`);
      console.log('data has been sorted, userContractPaymentDetails created...');
  
    //   const updateSheetPromise = updateGoogleSheet({
    //     spreadsheetId: pgmTransactionId,
    //     resource: {
    //       // How the input data should be interpreted.
    //       valueInputOption: 'RAW',  // TODO: Update placeholder value.
    //       // The new values to apply to the spreadsheet.
    //       data: [
    //         {
    //           range: `Migration Template!A2:CM`,
    //           majorDimension: "ROWS",
    //           values: transSheet
    //         }
    //       ],
    //     },
    //   });

      //return updateSheetPromise.then((result)=>{
        return res.status(200).send({
   
            success:true,
            // message: `Hello ${userEmail}`,
            // email:userEmail,
            // contentType: req.get('content-type'),
            // itemData:itemData,
            heapMemory:used2,
            used3:used3,
            used4,used4,
            used5:used5,
            used6:used6,
            // userContractPaymentDetails
            // pgmContractMapByUserId,
            // transactionInfoArray
        });
    // });

    });
});

// add all members to pgm to sheet
// exports.addKLCCMembersToPGMSheets = functions.https.onRequest((req, res) => {
functions.http('addklccmemberstopgmheets', (req, res)=>{
  const itemData = req.body;
  const userEmail = itemData && itemData.email;
  const itemUserId = itemData && itemData.userId;
  const userQuery = userEmail? admin.firestore().collection('users').where('email', '==', userEmail).get():admin.firestore().collection('users').get();
  const packageQuery = admin.firestore().collection('packages').get();
  const paymentQuery = itemUserId? admin.firestore().collection('payments').where('userId', '==', itemUserId).where('type', '==', 'membership').get():
    admin.firestore().collection('payments').where('type', '==', 'membership').get();
  const monthlyPackageQuery = admin.firestore().collection('pgmPackages').get();

  // return res.status(200).send({success:true});

  return Promise.all([userQuery, packageQuery, paymentQuery, monthlyPackageQuery]).then(results=>{
    const userRes = results[0];
    const pkgRes = results[1];
    const paymentRes = results[2];
    const monthlyPkgRes = results[3];
   
    var staffMap = {};
    var userSheets = [];
    var pkgMap = {};
    var paymentMap = {};
    var paymentArray = [];

    pkgRes.forEach(doc=>{pkgMap[doc.id]=doc.data()});

    var monthlyPkgMap = {}
    var monthlypkgMapByPrice = {};
    var monthlyPkgArray = [];
    monthlyPkgRes.forEach(doc=>{
      const data = doc.data();
      // const packageIds = data.packageIds;
      const price = data.price && parseFloat(data.price).toFixed(2);
      monthlypkgMapByPrice[price]=data;
      monthlypkgMapByPrice[price].id = doc.id;
      monthlyPkgMap[doc.id]=data;
      // packageIds && packageIds.forEach(packageId=>{
      //   monthlyPkgMap[]
      // });
    });

    paymentRes.forEach(doc=>{
      const data = doc.data();
      const userId = data.userId;
      const status = data.status;
      const freezeFor = data.freezeFor;
      const source = data.source;
      const type = data.type;

      if (((type && type==='membership') && (status && status === 'CLOSED')) || (freezeFor) 
         || (source && (source === 'join' || source === 'luckyDraw' || source === 'promo' || source === 'free' || source === 'complimentary' || source === 'jfr' || source === 'refer'))
        ){
        paymentArray = paymentMap[userId] || [];
        paymentArray.push({...data, paymentId:doc.id});
        paymentMap[userId]=paymentArray;
      }
    });

    var userPaymentDetails = {};
    userRes && userRes.forEach(doc=>{
      const data = doc.data();
      const packageId = data && data.packageId;
      const email = data && data.email;
      const name = data && data.name;
      const lastName = getLastName(data);
      const phone = data && data.phone;
      const createdAt = data && data.createdAt;
      const joinDate = data && data.joinDate;
      const icNumber = data && (data.nric || data.icNumber || data.passport);
      // const gender = data && data.gender;
      const gender = getGender(data);  
      const dateOfBirth = data && data.dateOfBirth;
      const image = data && data.image;
      const firstJoinVisit = data && data.firstJoinVisit;
      const clubId = firstJoinVisit? (firstJoinVisit === 'TTDI')? 101 : (firstJoinVisit === 'KLCC')? 102 : 101 : 101;
      const gantnerCardNumber = data && data.gantnerCardNumber;
      const membershipEnds = data && getMembershipEnd(data);
      const membershipStarts = data && getMembershipStart(data);
      const isActive = packageId && membershipEnds && membershipStarts && moment(getTheDate(membershipEnds)).isSameOrAfter(moment());
      const isGuest = !membershipEnds && !membershipStarts;
      const isMember = (packageId && membershipEnds && membershipStarts)? true:false;
      const automaticRenew = isActive? 1:0;
      const mcId = data && data.mcId;
      const remarks = data && data.remarks;
      const isStaff = data && data.isStaff;
      const roles = data && data.roles;
      const staffRole = data && data.staffRole;
      const paymentData = paymentMap[doc.id];
      const isTestEmail = email && (email.toLowerCase().includes('faizul') || email.toLowerCase().includes('billy'));
      const packageData = packageId && pkgMap[packageId];
      const packageBase = packageData && packageData.base;
      const isKLCCPkg = packageBase && (packageBase === 'KLCC');
      const cancellationDate = data && data.cancellationDate;
      const pgmUserUpdatedAt = data && data.pgmUserUpdatedAt;

      var userInfo = [];
      // not inclde duplicate name
      if (isMember 
        && name 
        // && !(name.includes('(d)') || (lastName && lastName.includes('(d)')) || (name.includes('duplicate')))
        // && gender 
        && !isStaff
        && !roles
        && !staffRole
        && !isGuest
        && paymentData
        && !isTestEmail
        && isKLCCPkg
        && !cancellationDate
        && isActive
        && !pgmUserUpdatedAt
      ){
        
         // sort the payment
         paymentData && paymentData.sort((a,b)=>{
          const createdA = a.createdAt;
          const createdB = b.createdAt;
          if(createdA < createdB){return 1}
          else if(createdB < createdA){return -1;}
          else{return 0}
        });
        paymentData && paymentData.reverse();

        var monthStartCount = 0;
        var monthEndCount = 1;
        var startMoment = moment(getTheDate(membershipStarts)).tz('Asia/Kuala_Lumpur').startOf('day');
        var endMoment = moment(getTheDate(membershipEnds)).tz('Asia/Kuala_Lumpur').startOf('day');

        var userFreezes = [];
        var userFreezeTerminated = [];
        var userFreeAccess = [];
        var membershipHistoryList = [];
        var combinedData = [];
        var combinedVendMth = [];
        var combinedTransactions = [];
        var addMonths = 0;
        var addYears = 0;
        
        var paymentCount = 0;
        paymentData && paymentData.forEach((payData, index)=>{
          const freezeFor = payData.freezeFor;
          const packageId = payData.packageId;
          const paymentCreatedDate = payData.createdAt;
          const renewalTerm = (payData && payData.renewalTerm) || 'month';
          const source = payData.source;
          const qty = payData.quantity||1;
          const cardSummary = payData.cardSummary;
          const cardExpired = payData.cardExpiryDate;
          const price = payData.totalPrice? payData.totalPrice:0;
          const status = payData.status;
          const type = payData.type;
          const paymentType = payData.paymentType;
          const userId = payData.userId;
          const paymentId = payData.paymentId;
       
          // 1. need to sort the freeze first
          if (source && freezeFor){
            const freezeType = payData.freezeType;
            for (var a = 0; a<qty; a++){
              userFreezes.push({
                date:moment(getTheDate(freezeFor)).add(a, 'months'),
                freezeType,
                yearOfFreeze: moment(freezeFor).format('YYYY'),
                cardSummary, cardExpired, paymentId,
                ...payData,
                isFreeze:true
                // need to link with contract?
              });
            }
          }
          else if (source==='freezeTerminate'){
            userFreezeTerminated.push({date:moment(freezeFor), paymentId});
          }
          else if ((source==='join') || (source==='luckyDraw') || (source==='promo')|| (source==='free')
          || (source==='complimentary') || (source==='jfr') || (source==='refer') 
          || (parseInt(price)===0)
          ){
            userFreeAccess.push({
              date:moment(getTheDate(paymentCreatedDate)), type:source, paymentId, 
              ...payData,
              isFree:true
            });
          }

          else if (((source === 'vend') || ((source === 'adyen') && (parseInt(price)!=0)) || (source==='pbonline'))
          && (status === 'CLOSED') && (type === 'membership')){
            for (var h=0; h<qty; h++){
              combinedTransactions.push({
                ...payData,
                cycle:h+1
              })
              // combinedVendMth.push({
              //   date:moment(paymentCreatedDate).add(i, 'months'), 
              //   paymentDate:paymentCreatedDate,
              //   type:source,
              //   price, paymentType, cardSummary, cardExpired,
              //   packageId, paymentId, renewalTerm, 
              //   ...payData
              // });
            }
            // for the combined transactions
           
            var monthlyPkgPrice = 0;
            var monthlyPkgData;
            if (renewalTerm && (renewalTerm === 'year' || renewalTerm === 'yearly')){
              monthlyPkgPrice = price && parseFloat(price/12*qty).toFixed(2);
              monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
              const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
              const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
    
              for (var i=0; i<(qty*12); i++){
                combinedVendMth.push({
                  date:moment(getTheDate(paymentCreatedDate)).add(i, 'months'), 
                  paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                  type:source,
                  cycle:i+1,
                  ...payData,
                  ...monthlyPkgData,
                  monthlyPkgId,
                  monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                  // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                });
                // if (cardSummary){
                //   combinedVendMth[i].cardSummary = `${cardSummary} ${cardExpired}`;
                // }
              }
            }
            else if (renewalTerm && (renewalTerm === 'month' || renewalTerm === 'monthly')){
              monthlyPkgPrice = price && parseFloat(price/qty).toFixed(2);
              monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
              const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
              const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
              for (var j=0; j<qty; j++){
                combinedVendMth.push({
                  date:moment(getTheDate(paymentCreatedDate)).add(j, 'months'),
                  paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                  type:source,
                  cycle:j+1,
                  ...payData,
                  ...monthlyPkgData,
                  monthlyPkgId,
                  monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                  // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=220)? true:false
                  isFree: (price && parseInt(price) === 0)? true:false
                });
                // if (cardSummary){
                //   combinedVendMth[j].cardSummary = `${cardSummary} ${cardExpired}`;
                // }
              }
            }
            else if (renewalTerm && (renewalTerm === 'biyearly' || renewalTerm === 'biyear')){
              monthlyPkgPrice = price && parseFloat(price/6*qty).toFixed(2);
              monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
              const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
              const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
              for (var k=0; k<qty*6; k++){
                combinedVendMth.push({
                  date:moment(getTheDate(paymentCreatedDate)).add(k, 'months'), 
                  paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                  cycle:k+1,
                  ...payData,
                  ...monthlyPkgData,
                  monthlyPkgId,
                  monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                  // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                });
                // if (cardSummary){
                //   combinedVendMth[k].cardSummary = `${cardSummary} ${cardExpired}`;
                // }
              }
            }
            else if (renewalTerm && renewalTerm === 'quarterly'){
              monthlyPkgPrice = price && parseFloat(price/3*qty).toFixed(2);
              monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
              const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
              const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
              for (var l=0; l<qty*3; l++){
                combinedVendMth.push({
                  date:moment(getTheDate(paymentCreatedDate)).add(l, 'months'), 
                  paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                  cycle:l+1,
                  ...payData,
                  ...monthlyPkgData,
                  monthlyPkgId,
                  monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                  // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                });
                // if (cardSummary){
                //   combinedVendMth[l].cardSummary = `${cardSummary} ${cardExpired}`;
                // }
              }
            }
            else if (renewalTerm && renewalTerm === '4monthly'){
              monthlyPkgPrice = price && parseFloat(price/4*qty).toFixed(2);
              monthlyPkgData = monthlyPkgPrice && monthlypkgMapByPrice[monthlyPkgPrice];
              const pkgIds = monthlyPkgData && monthlyPkgData.packageIds;
              const monthlyPkgId = monthlyPkgData && monthlyPkgData.id;
              for (var m=0; m<qty*4; m++){
                combinedVendMth.push({
                  date:moment(getTheDate(paymentCreatedDate)).add(m, 'months'), 
                  paymentDate:moment(getTheDate(paymentCreatedDate)).format('YYYY-MM-DD'),
                  cycle:m+1,
                  ...payData,
                  ...monthlyPkgData,
                  monthlyPkgId,
                  monthlyPkgPrice:monthlyPkgPrice?monthlyPkgPrice:0,
                  // isFree: (!monthlyPkgData && parseFloat(monthlyPkgPrice)>0 && parseFloat(monthlyPkgPrice)<=90)? true:false
                });
                // if (cardSummary){
                //   combinedVendMth[m].cardSummary = `${cardSummary} ${cardExpired}`;
                // }
              }
            }
            // for complimentary
            else if (renewalTerm && renewalTerm === 'never'){

            }
          }

          // resort the array dates
          userFreezes.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
          userFreezes.reverse();
          userFreezeTerminated.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
          userFreezeTerminated.reverse();
          userFreeAccess.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
          userFreeAccess.reverse();
          combinedVendMth.sort((a,b) => a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD'));
          combinedVendMth.reverse();

          const initialMonthsDiff = Math.max(moment(new Date()).diff(startMoment, 'months'));
          var monthsDiff = initialMonthsDiff;
          var totalArrayLength = userFreezeTerminated.length + userFreezes.length + userFreeAccess.length + combinedVendMth.length;
          if (totalArrayLength>initialMonthsDiff){
            monthsDiff = totalArrayLength-1;
          }

          // default, if there is no payment detected
          for (var i=0; i<=monthsDiff; i++){
            const iterationStartMoment = startMoment.clone().add(i, 'months');
            // combinedItems.push({effectiveDate:iterationStartMoment, primaryText:primaryText, secondaryText: secondaryText, action:action});
            membershipHistoryList.push({
              iterationDate:iterationStartMoment.format('YYYY-MM-DD'), 
              count:i,
              userId
              // addMonths, addYears,
              // startMoment:startMoment && startMoment.format('YYYY-MM-DD')
            });
          }

          membershipHistoryList && membershipHistoryList.forEach((x,indexx)=>{
            if (userFreezeTerminated && userFreezeTerminated.length>0 
              && moment(x.iterationDate).isSameOrAfter(userFreezeTerminated[userFreezeTerminated.length-1].date)
              // && moment(userFreezeTerminated[userFreezeTerminated.length-1].date).isBetween(x.date, moment(x.date).add(1,'month'))
              // && moment(x.date).isBetween(userFreezeTerminated[userFreezeTerminated.length-1].date, userFreezeTerminated[userFreezeTerminated.length-1].date.add('months', 1))
            ){
                combinedData.push({
                  date:userFreezeTerminated[userFreezeTerminated.length-1].date,
                  type:'freezeTerminated',
                  index:indexx,
                  ...userFreezeTerminated[userFreezeTerminated.length-1]
                });
                userFreezeTerminated.pop();
            }
            else if (userFreezes && userFreezes.length>0
              && moment(x.iterationDate).isSameOrAfter(userFreezes[userFreezes.length-1].date.clone()) 
              && moment(x.iterationDate).isBefore(userFreezes[userFreezes.length-1].date.clone().add(1, 'months')) 
              // && moment(userFreezes[userFreezes.length-1].date).isBetween(x.date, (x.date).add('month', 1).subtract('days', 1))
              // && moment(x.date).isBetween(userFreezes[userFreezes.length-1].date, userFreezes[userFreezes.length-1].date.add('months', 1))
              ){
              combinedData.push({
                // date:userFreezes[userFreezes.length-1].date,
                // type:'freeze',
                // freezeType: userFreezes[userFreezes.length-1].freezeType? userFreezes[userFreezes.length-1].freezeType:null,
                // price:userFreezes[userFreezes.length-1].price, 
                // freezeCountPerYear:userFreezes[userFreezes.length-1].freezeCountPerYear,
                index:indexx,
                // paymentId:userFreezes[userFreezes.length-1].paymentId,
                ...userFreezes[userFreezes.length-1]
              });
              userFreezes.pop();
            }
            else if (userFreeAccess && userFreeAccess.length>0 
              // && userFreeAccess[userFreeAccess.length-1].date.isSameOrAfter(moment(x.date))
              && moment(x.iterationDate).isSameOrAfter(userFreeAccess[userFreeAccess.length-1].date)
              // && userFreeAccess[userFreeAccess.length-1].date.isBetween(x.date, moment(x.date).add(1,'month'))
              ){
              combinedData.push({
                // date:userFreeAccess[userFreeAccess.length-1].date,
                // type:userFreeAccess[userFreeAccess.length-1].type,
                index:indexx,
              //  paymentId:userFreeAccess[userFreeAccess.length-1].paymentId,
               ...userFreeAccess[userFreeAccess.length-1]
              })
              userFreeAccess.pop();   
            }
            else if (combinedVendMth && combinedVendMth.length>0){
              combinedData.push({
                // date:combinedVendMth[combinedVendMth.length-1].date,
                // paymentDate: combinedVendMth[combinedVendMth.length-1].paymentDate,
                // type:combinedVendMth[combinedVendMth.length-1].type,
                // // visitLeft:combinedVendMth[combinedVendMth.length-1].visitLeft,
                // // visitMax: combinedVendMth[combinedVendMth.length-1].visitMax,
                // price:combinedVendMth[combinedVendMth.length-1].price, 
                // paymentType: combinedVendMth[combinedVendMth.length-1].paymentType,
                // cardSummary: combinedVendMth[combinedVendMth.length-1].cardSummary,
                // cardExpired: combinedVendMth[combinedVendMth.length-1].cardExpired,
                // index:indexx,
                // packageId:combinedVendMth[combinedVendMth.length-1].packageId,
                // paymentId:combinedVendMth[combinedVendMth.length-1].paymentId,
                // renewalTerm: combinedVendMth[combinedVendMth.length-1].renewalTerm,
                index:indexx,
                ...combinedVendMth[combinedVendMth.length-1]
              })
              combinedVendMth.pop();
            }
           
            else{
              // combinedData.push({
              //   ...x,
              //   test: 'outside of loop',
              //   iterationDate:x.iterationDate
              // })
            }
          });

          // sorting...
          combinedData.sort((a,b) => {
            const totalPriceA = a.totalPrice? parseInt(a.totalPrice):0;
            const totalPriceB = b.totalPrice? parseInt(a.totalPrice):0;
            const dateA = a.date.format('YYYY-MM-DD');
            const dateB = b.date.format('YYYY-MM-DD');
            if (dateA>dateB){
              return 1
              // if (totalPriceA>totalPriceB){
              //   return 1;
              // }
              // else if (totalPriceA<totalPriceB){
              //   return -1;
              // }
              // else{
              //   return 1
              // }
            }
            else if (dateA<dateB){
              return -1;
              // if (totalPriceA>totalPriceB){
              //   return -1;
              // }
              // else if (totalPriceA<totalPriceB){
              //   return 1;
              // }
              // else{
              //   return -1
              // }
            }
            else if (dateA === dateB){
              if (totalPriceA>totalPriceB){
                return 1;
              }
              else if (totalPriceA<totalPriceB){
                return -1;
              }
              else{
                return 0
              }
            }
            else {
              return 0
            }

            // if (dateA === dateB)
            // a.date.format('YYYYMMDD') - b.date.format('YYYYMMDD')
            // const createdA = a.createdAt;
            // const createdB = b.createdAt;
            // if(createdA < createdB){return 1}
            // else if(createdB < createdA){return -1;}
            // else{return 0}
          });
          combinedTransactions.sort((a,b)=>a.createdAt - b.createdAt);

        });

        var contractLength = 0;
        var refMonthCount = 0;
        var paidMonthCount = 0;
        var paymentsByContract = [];
        var contractpackageID = null;
        var contractCounter = 0;
        var freezeCount = 0;
        var freeMonthCount = 0;
        var startDate;
        var endDate;
        var transactionsByContract=[];
        var combinedTotalPrice = 0;

        membershipHistoryList.forEach((x, indexx, array)=>{
        // combinedData.forEach((x, indexx, array)=>{
          // else if (!userPaymentDetails[doc.id][contractCounter]['paymentId']){
          //   // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
          //   userPaymentDetails[doc.id][contractCounter]['paymentId']={}; // default
          // }

          if (!userPaymentDetails){
            userPaymentDetails={}
          }
          if (!userPaymentDetails[doc.id]){
            userPaymentDetails[doc.id]={
              totalContracts:0
            }; // test first
          }
          if (!userPaymentDetails[doc.id]['totalContracts']){
            userPaymentDetails[doc.id]['totalContracts'] = 0;
          }
          if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
            // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
                // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
            // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
            userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
              packageId:'', // packageId of the current contract
              monthlyPkgId:'',
              startDate:'', // start date of the current contract
              endDate:'', // endDate of the current contract
              payments:[], // payments for the contract
              contractLength: 0, // month count
              freezeCount:0,
              freeMonthCount:0,
              paidMonthCount:0,
              refMonthCount:0,
              combinedTotalPrice:0,
              // monthlyPkgPrice:0
            }; // default
          }
          if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']){
            // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
                // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
            // userPaymentDetails[doc.id][contractCounter]={combinedData}; 
            userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']=[]; // default
          }

          userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['userId'] = x.userId;
          userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['userData'] = data;
          userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['userData']['lastName'] = lastName;

          if (!startDate){ // default
            startDate = moment(x.iterationDate).format('YYYY-MM-DD');
          }

          if (combinedData && combinedData.length>0 && indexx<combinedData.length){
            if (combinedData[indexx].totalPrice && parseFloat(combinedData[indexx].totalPrice)>0 && !combinedData[indexx].isFree){
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['combinedTotalPrice'] += parseFloat(combinedData[indexx].totalPrice);
            }

            if (combinedData[indexx].monthlyPkgId && (!contractpackageID || (contractpackageID===combinedData[indexx].monthlyPkgId))
              && (!combinedData[indexx].isFree) && (!(combinedData[indexx].type && combinedData[indexx].type === 'freezeTerminated'))
              ){
              if (!contractpackageID){
                contractpackageID = combinedData[indexx].monthlyPkgId;
                startDate = startMoment.format('YYYY-MM-DD');
              }
              if ((startDate && contractLength === 0) || !startDate){ // for the next contract
                startDate = moment(x.iterationDate).format('YYYY-MM-DD');
              }
              if (!endDate){
                endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              }
              else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              }
              
              // endDate = moment(startDate).add(contractLength+1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              // if (moment(x.iterationDate).isAfter(startDate) && ){
              //   startDate = x.date;
              // }
              contractLength++;
              paidMonthCount++;
              // userPaymentDetails[doc.id][contractCounter]['startDate'] = moment(startDate).format('YYYY-MM-DD'); // need to have 1 more loop?
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = startDate;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['packageId'] = contractpackageID;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['monthlyPkgId'] = contractpackageID;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['paidMonthCount'] = paidMonthCount;
              // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`][`monthlyPkgPrice`]
              paymentsByContract.push({
                // date:array[indexx].date,
                ...x,
                ...combinedData[indexx],
                payType:'paid',
                paidMonthCount,
                endDate
              });
              transactionsByContract.push({
                ...combinedTransactions[indexx]
              });
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['transactions'] = transactionsByContract;
              // remove the combinedData to avoid repetition
              // combinedData.splice(indexx, 1); 
            }
            // for referral
            else if(!combinedData[indexx].monthlyPkgId && combinedData[indexx].source && combinedData[indexx].source === 'refer'){
              refMonthCount++;
              contractLength++
              paymentsByContract.push({
                ...x,
                ...combinedData[indexx],
                payType:'refer',
                endDate
              });
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['refMonthCount'] = refMonthCount;
              if (!endDate){
                endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              }
              else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              }
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
            }
            // for free
            else if ((combinedData[indexx].monthlyPkgId && combinedData[indexx].totalPrice && (parseInt(combinedData[indexx].totalPrice)===0 && contractpackageID && combinedData[indexx].isFree))
              || (combinedData[indexx].totalPrice && parseInt(combinedData[indexx].totalPrice)===0 && combinedData[indexx].source && combinedData[indexx].source != 'freeze')
              || (contractpackageID && combinedData[indexx].isFree)
            ){ // for free, use the current contract packageId
              paymentsByContract.push({
                ...x,
                ...combinedData[indexx],
                payType:'free',
                startDate,
                endDate
              });
              if (!userPaymentDetails){
                userPaymentDetails={}
              }
              if (!userPaymentDetails[doc.id]){
                userPaymentDetails[doc.id]={}; // test first
              }
              if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                  pgmContractId:'',
                  pgmTransactions:{},
                  packageId:'', // packageId of the current contract
                  monthlyPkgId:'', // monthly pkgId
                  startDate:'', // start date of the current contract
                  endDate:'', // endDate of the current contract
                  payments:[], // payments for the contract
                  contractLength: 0, // month count
                  freezeCount:0,
                  freeMonthCount:0,
                  refMonthCount:0
                }; // default
              }
              if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']){
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']=[]; // default
              }
              contractLength++;
              freeMonthCount++;
              if (!endDate){
                endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              }
              else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              }
              if (userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] === ""){ // for empty string
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = startDate;
              }
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['freeMonthCount'] = freeMonthCount;
            }
            // temporary disable
            // else if (combinedData[indexx].source && (combinedData[indexx].source.includes('freezeTerminate'))){ // for freezeTerminate
            //   // skip?
            //   // if (startDate && contractLength === 0){ // for the next contract
            //   //   startDate = moment(x.iterationDate).format('YYYY-MM-DD');
            //   // }
            //   contractLength = 0;
            //   // contractCounter++;
            //   freeMonthCount=0;
            //   freezeCount=0;
            //   paidMonthCount=0;
            //   refMonthCount=0;

            // }
            else if (combinedData[indexx].source && (combinedData[indexx].source.includes('reeze'))){ // for freeze & specialFreeze
              freezeCount++;
              contractLength++
              if (!endDate){
                endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              }
              else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              }
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
              paymentsByContract.push({
                ...x,
                ...combinedData[indexx],
                payType:'freeze',
                endDate
              });
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['freezeCount'] = freezeCount;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
               // remove the combinedData to avoid repetition
               // combinedData.splice(indexx, 1); 
            }

            else if (combinedData[indexx].monthlyPkgId && (combinedData[indexx].monthlyPkgId != contractpackageID)){ 
              // for new contract
              contractLength = 0;
              contractCounter++;
              freeMonthCount=0;
              freezeCount=0;
              paidMonthCount=1;
              refMonthCount=0;

              if (!userPaymentDetails){
                userPaymentDetails={}
              }
              if (!userPaymentDetails[doc.id]){
                userPaymentDetails[doc.id]={}; // test first
              }
              if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                  packageId:'', // packageId of the current contract
                  monthlyPkgId:'', // monthly packageId
                  startDate:'', // start date of the current contract
                  endDate:'', // endDate of the current contract
                  payments:[], // payments for the contract
                  contractLength: 0, // month count
                  freezeCount:0,
                  freeMonthCount:0,
                  refMonthCount:0,
                  paidMonthCount:1,
                  combinedTotalPrice:0
                }; // default
              }
              if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']){
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments']=[]; // default
              }
              if (combinedData[indexx].totalPrice && parseFloat(combinedData[indexx].totalPrice)>0 && !combinedData[indexx].isFree){
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['combinedTotalPrice'] += parseFloat(combinedData[indexx].totalPrice);
              }
              contractLength++
              contractpackageID = combinedData[indexx].monthlyPkgId; // replace with the new packageId
              startDate = moment(x.iterationDate).format('YYYY-MM-DD');
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = startDate;
             
              // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['startDate'] = moment(x.date).format('YYYY-MM-DD'); // need to have 1 more loop?
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['contractLength'] = contractLength;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['packageId'] = contractpackageID;
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['monthlyPkgId'] = contractpackageID;
             // reset the payments
              paymentsByContract = [{
                ...x,
                // date:array[indexx].date,
                ...combinedData[indexx],
                endDate
              }];
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
              if (!endDate){
                endDate = moment(startDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              }
              else if (endDate && moment(x.iterationDate).isSameOrAfter(moment(endDate))){
                endDate = moment(x.iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD');
              }
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['endDate'] = endDate;
               // remove the combinedData to avoid repetition
               // combinedData.splice(indexx, 1); 
              // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'].push({
              //   ...x,
              //   ...combinedData[indexx]
              // })
            }
            else{
              contractLength++;
              if (!userPaymentDetails){
                userPaymentDetails={}
              }
              if (!userPaymentDetails[doc.id]){
                userPaymentDetails[doc.id]={}; // test first
              }
              if (!userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]){
                // userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={combinedData}; 
                userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]={
                  packageId:'', // packageId of the current contract
                  monthlyPkgId:'', // monthly packageId
                  startDate:'', // start date of the current contract
                  endDate:'', // endDate of the current contract
                  payments:[], // payments for the contract
                  contractLength: 0, // month count
                  freezeCount:0,
                  freeMonthCount:0,
                  refMonthCount:0,
                  paidMonthCount:0
                }; // default
              }
              paymentsByContract.push({
                // date:x.date,
                ...x,
                ...combinedData[indexx],
                test:'unknown',
                endDate,
                startDate
              });
              userPaymentDetails[doc.id][`${doc.id}_${contractCounter}`]['payments'] = paymentsByContract;
            }
          }
          userPaymentDetails[doc.id]['totalContracts']=contractCounter+1;
          userPaymentDetails[doc.id]['allPayments']=combinedData;

        });
      }
    });

    var userInfo = [];

    Object.entries(userPaymentDetails).forEach(([userId, contractValue]) => {
      Object.entries(contractValue).forEach(([contractCounter, contractValue]) => {
        const userData = contractValue && contractValue.userData;
        const packageId = userData && userData.packageId;
        const packageData = packageId && pkgMap[packageId];
        const clubId = getClubIdByUserData(userData, packageData);
        const mcId = userData && userData.mcId;
        const consultantData = mcId && staffMap[mcId];
        const consultantEmail = consultantData && consultantData.email;
        const membershipEnds = userData && getMembershipEnd(userData);
        const membershipStarts = userData && getMembershipStart(userData);
        const isActive = packageId && membershipEnds && membershipStarts && moment(getTheDate(membershipEnds)).isSameOrAfter(moment());
        const isGuest = !membershipEnds && !membershipStarts;
        const isMember = (packageId && membershipEnds)? true:false;
        const pkgBase = packageData && packageData.base;
        const pkgName = packageData && packageData.name;
        const payments = contractValue && contractValue.payments;
        const remarks = userData && userData.remarks;
        const combinedTotalPrice = contractValue.combinedTotalPrice;
        const freezeCount = contractValue.freezeCount;
        var paymentType = 'Cash';
        const monthlyPkgId =  contractValue && contractValue.monthlyPkgId;
        const monthlyPkgData = monthlyPkgId && monthlyPkgMap[monthlyPkgId];
        const monthlyPkgPrice = monthlyPkgData && monthlyPkgData.price;
        const monthlyPkgName = monthlyPkgData && monthlyPkgData.name;

        // payments && payments.forEach(payment=>{
        //   const iterationDate = payment.iterationDate;
        //   const totalPrice = payment.totalPrice;
        //   const paymentType = payment.paymentType? payment.paymentType:'Cash';

          if (userId && (contractValue.packageId || (contractValue.startDate && contractValue.endDate)) && userData && membershipStarts && membershipEnds){
            userInfo = [
              userId? `${userId}`:'', // external userId
              // contractValue.monthlyPkgId? contractValue.monthlyPkgId:contractValue.packageId? contractValue.packageId:'',// external contractId
              contractCounter? contractCounter:'',
              userData? userData.name? userData.name:'':'',
              userData? userData.lastName? userData.lastName:'':'',
              '', //secondName
              userData? userData.gender? userData.gender.charAt(0).toUpperCase() + userData.gender.slice(1):'Female':'Female',
              userData? userData.dateOfBirth? getTheDateFormat(userData.dateOfBirth, 'YYYY-MM-DD'):'':'',
              userData? userData.email? userData.email:'':'',
              '', // personal Id
              '', // member home phone
              userData? userData.phone? userData.phone:'':'', // mobilePhone
              userData? userData.image? userData.image:'':'', // photoURL
              '', // company
              '', // street
              '', // postalcode
              '', // city
              '', // state
              'Malaysia', // default?
              userData? userData.icNumber? userData.icNumber:'':'', // DocumentNumber
              '', // DocumentIssuedBy
              '', // DocumentIssueDate
              '', // UserNumber
              clubId, // clubId
              userData? userData.gantnerCardNumber? userData.gantnerCardNumber:'':'', // MemberCardNumber
              consultantEmail? consultantEmail:'', // Consultant
              isActive? 1:0,
              isGuest? 1:0,
              '', // TodayBalance
              '', // PrepaidBalance
              '', // signupdate
              contractValue.startDate?contractValue.startDate:'', // startDate,
              contractValue.endDate? contractValue.endDate:'',// endDate? endDate:'', // endDate,
              // iterationDate? iterationDate:'',
              // iterationDate? moment(iterationDate).add(1, 'months').subtract(1, 'days').format('YYYY-MM-DD'):'',
              '', //MinCancelTimeMonths
              0, // IsProrata, Yes, if contract user
              '', // IsEndProrata
              '', // ProrataDay
              (pkgBase === 'KLCC')? 'multi club access rules': 'single club access rules', // Membership with given name must exist in PG system
              // pkgName, // paymentPlan
              monthlyPkgName? monthlyPkgName:'Complimentary', // paymentPlan
              'contract', // paymentPlan
              '', // visitLimit
              '', // RemainingVisitCount
              '', // VisitPeriod
              '', // SynchronizeWithContract
              isMember? 'Month':'', // Timeperiod. Is contract time interval based on days or months
              (contractValue && contractValue.contractLength)? contractValue.contractLength:'', // monthQty, // monthDiff, // ContractLength
              1, // monthDiff, // ContractFrequency
              '', // IsUpfront
              6, // VatRate, SST?
              '', // AdminFee
              '', // AdminFee VatRate
              // totalPrice? totalPrice:0, // membershipFee, put 0?
              // combinedTotalPrice?combinedTotalPrice:0,
              monthlyPkgPrice?monthlyPkgPrice:0,
              paymentType, // paymentType
              '', // StartChargingFromDate
              '', // StartGeneratingTransactionsFromDate
              '', // ForcePaymentPlan
              '', // PaymentPlanAutoName
              '', // IsPaymentChosenDay
              '', // IsAutomaticallyEnded
              '', // IsAdditionalContract
              '', // StopChargingAfterMinPeriod
              freezeCount? freezeCount>0? 1:0:0, // FreezeAvailable
              '', // AutomaticRenew																					
              userData? userData.name? userData.name:'':'', // PayerName, default to userName
              '', // PayerAddress
              '', // PayerPostalCode
              '', // PayerCity
              '', // AccountNumber
              '', // VirtualAccountNumber
              '', // BankAccountBic
              '', // BankAccountMandatoryId
              '', // BankAccountFirstPayment
              '', // BankAccountMandatorySignUpDate
              '', // CreditCardNumber
              '', // CreditCardReferenceNumber
              '', // CreditCardExpityDate
              remarks? remarks:'', // Comment1
              '', // Comment2
              '', // Comment3
              '', // Comment4
              '', // Tags

              // name? name:'',
              // lastName? lastName:'',
              // '', //secondName
              // gender? gender:'', //sex
              // dateOfBirth? getTheDateFormat(dateOfBirth, 'YYYY-MM-DD'):'', //birthOfDate
              // email? email:'', // email
              // '', // personal Id
              // '', // member home phone
              // phone? phone:'', //mobilePhone
              // image? image:'', //photoURL
              // '', // company
              // '', // street
              // '', // postalcode
              // '', // city
              // '', // state
              // 'Malaysia', // default?
              // icNumber? icNumber:'', // DocumentNumber
              // '', // DocumentIssuedBy
              // '', // DocumentIssueDate
              // '', // UserNumber
              // clubId? clubId:'', // ClubNumber
              // gantnerCardNumber? gantnerCardNumber:'', // MemberCardNumber
              // consultantEmail? consultantEmail:'', // Consultant
              // isActive? 1:0,
              // isGuest? 1:0,
              // '', // TodayBalance
              // '', // PrepaidBalance
              // '', // signupdate
              // '',// startDate? startDate:'', // startDate,
              // '',// endDate? endDate:'', // endDate,
              // '', //MinCancelTimeMonths
              // isActive? 1:0, // IsProrata, Yes, if contract user
              // '', // IsEndProrata
              // '', // ProrataDay
              // (pkgBase === 'KLCC')? 'multi club access rules': 'single club access rules', // membership, multi club access rules, single club access rules
              // pkgName, // paymentPlan
              // 'contract', // paymentPlan
              // '', // visitLimit
              // '', // RemainingVisitCount
              // '', // VisitPeriod
              // '', // SynchronizeWithContract
              // isMember? 'Month':'', // Timeperiod. Is contract time interval based on days or months
              // monthQty, // monthDiff, // ContractLength
              // 1, // monthDiff, // ContractFrequency
              // '', // IsUpfront
              // 6, // VatRate, SST?
              // '', // AdminFee
              // '', // AdminFee VatRate
              // totalPrice? totalPrice:0, // membershipFee, put 0?
              // 'Cash', // paymentType
              // '',
              // '', // StartChargingFromDate
              // '', // StartGeneratingTransactionsFromDate
              // '', // ForcePaymentPlan
              // '', // PaymentPlanAutoName
              // '', // IsPaymentChosenDay
              // '', // IsAutomaticallyEnded
              // '', // IsAdditionalContract
              // '', // StopChargingAfterMinPeriod
              // '', // FreezeAvailable
              // '', // AutomaticRenew																					
              // '', // PayerName
              // '', // PayerAddress
              // '', // PayerPostalCode
              // '', // PayerCity
              // '', // AccountNumber
              // '', // VirtualAccountNumber
              // '', // BankAccountBic
              // '', // BankAccountMandatoryId
              // '', // BankAccountFirstPayment
              // '', // BankAccountMandatorySignUpDate
              // '', // CreditCardNumber
              // '', // CreditCardReferenceNumber
              // '', // CreditCardExpityDate
              // remarks? remarks:'', // Comment1
              // '', // Comment2
              // '', // Comment3
              // '', // Comment4
              // '', // Tags
              // // this line and below need to remove
              // // renewalTerm?renewalTerm:'null',
              // // paymentCount,
              // // monthStartCount,
              // // monthEndCount
            ];
            userSheets.push(userInfo);
          }
          else{
            // console.log('condition not valid: ', contractValue);
          }

        // });

     
      });
    });

    // const updateSheetPromise = updateGoogleSheet({
    //   // spreadsheetId: pgmAllMembersId,
    //   spreadsheetId: pgmAllMembers2Id,
    //   resource: {
    //     // How the input data should be interpreted.
    //     valueInputOption: 'RAW',  // TODO: Update placeholder value.
    //     // The new values to apply to the spreadsheet.
    //     data: [
    //       {
    //         range: `Migration Template!A2:CM`,
    //         majorDimension: "ROWS",
    //         values: userSheets
    //       }
    //     ],
    //   },
    // });

    // Get a new write batch
    var batch = admin.firestore().batch();

    return res.status(200).send({success:true});

    // return updateSheetPromise.then((result)=>{
    //   // Object.entries(userPaymentDetails).forEach(([userId, contractValue]) => {
    //   //   Object.entries(contractValue).forEach(([contractCounter, contractValue]) => {
    //   //     const userData = contractValue && contractValue.userData;
    //   //     const packageId = userData && userData.packageId;

    //   //     // Set the value of 'PGUsers'
         
    //   //   });
    //   //   // const pgUsersRef = admin.firestore().collection('pgUsers').doc(userId);
    //   //   // batch.set(pgUsersRef, {...contractValue, createdAt:timestamp});
    //   // });

    //   // return batch.commit().then(()=>{
    //       return res.status(200).send({
    //         success:true,
    //         result:result,
    //         // userPaymentDetails:userPaymentDetails?userPaymentDetails:null,
    //         // monthlypkgMapByPrice,
    //       })
    //   // });     
    // });
  });
});
// /**
//  * Responds to an HTTP request using data from the request body parsed according
//  * to the "content-type" header.
//  *
//  * @param {Object} req Cloud Function request context.
//  * @param {Object} res Cloud Function response context.
//  */
//  exports.helloContent = (req, res) => {
//     let name;
  
//     switch (req.get('content-type')) {
//       // '{"name":"John"}'
//       case 'application/json':
//         ({name} = req.body);
//         break;
  
//       // 'John', stored in a Buffer
//       case 'application/octet-stream':
//         name = req.body.toString(); // Convert buffer to a string
//         break;
  
//       // 'John'
//       case 'text/plain':
//         name = req.body;
//         break;
  
//       // 'name=John' in the body of a POST request (not the URL)
//       case 'application/x-www-form-urlencoded':
//         ({name} = req.body);
//         break;
//     }
  
//     res.status(200).send(`Hello ${escapeHtml(name || 'World')}!`);
//   };