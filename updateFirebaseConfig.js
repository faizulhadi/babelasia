#!/usr/bin/env node

'use strict'

const config = {
  hosting: {
    public: "build",
    ignore: [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ],
    headers: [
      { "source":"/service-worker.js", "headers": [{"key": "Cache-Control", "value": "no-cache"}] }
    ],
    rewrites: [
      {
        "source": "/createPaymentSession", "function": "createPaymentSession"
      },
      {
        "source": "**",
        "destination": "/index.html"
      }
    ],
    redirects:[]
  },
  functions: {
    predeploy: [
      "npm --prefix \"$RESOURCE_DIR\" run lint"
    ]
  }
}

const fs = require('fs')

const path = require('path')
const folders = fs.readdirSync(path.resolve(__dirname, 'build/static'))
folders.forEach((folder) => {
  const files = fs.readdirSync(path.resolve(__dirname, 'build/static', folder))
  files.forEach((file) => {
    if(file.indexOf('.png') === -1){
      config.hosting.redirects.push({
        source: `/static/${folder}/${file.replace(/^main\.(\w+)/, 'main.!($1)')}`,
        destination: `/static/${folder}/${file}`,
        type: 301
      })
    }
  })
})

fs.writeFileSync('firebase.json', JSON.stringify(config))
